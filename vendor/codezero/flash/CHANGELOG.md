# Changelog

All Notable changes to `Flash` will be documented in this file.

## 1.1.0 (2017-06-04)

- Enable package auto-discovery for Laravel 5.5 

## 1.0.0 (2015-05-15)

- Version 1.0.0 of Flash for Laravel 5.
- Flash multiple messages to the session.
- Optionally pass in a translator key to fetch a message translation.
- Add `dismissible` option for alerts (defaults to `true`)

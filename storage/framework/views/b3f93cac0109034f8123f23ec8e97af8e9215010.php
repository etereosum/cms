	<div  class="container-fluid " style="background:#08743b;">
			<?php echo $__env->make('public.formulariosModales.login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<div class="row">
		    <div class="col-md-8 offset-md-2" >
				<nav class="main-menu navbar-light">

					<a href="<?php echo e(url('/')); ?>" class="active">Inicio</a>
					<a href="<?php echo e(route('quienes-somos.index2')); ?>">Quiénes somos</a>
					<a href="<?php echo e(route('normativa_public.index')); ?>">Normativa</a>
					<a href="<?php echo e(route('contactos.index')); ?>">Contactos</a>

						<?php if(Auth::user()): ?>

							<a href="<?php echo e(route('dash.index')); ?>" target="_blank">Admin.. <?php echo e(Auth::user()->nombres); ?></a>

						<?php else: ?>

							<a href="#" data-toggle="modal" data-target="#login">Ingresar</a>

						<?php endif; ?>
				</nav>
			</div>
		</div>
	</div> 
<?php /**PATH /Users/pablo/Documents/misproyectos/freelance/asodir/proyectoasodir/resources/views/public/template/navbar_propia.blade.php ENDPATH**/ ?>
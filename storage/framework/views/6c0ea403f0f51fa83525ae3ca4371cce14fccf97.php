
<div id="notice">
	<section class="pt-4 ml-3">
			<header class="font-weight-bold text-uppercase"><h4>{{ noticia.titulo }}</h4></header>
			<span class="text-muted" style="font-size: 13.5px; font-family: Verdana;">Publicado: 
			{{ noticia.created_at | DDMMYYYY}}</span><br>
		<hr>
						
			<img :src="noticia.img_ruta" width="100%" height="300" alt="" class="text-justify">
	</section>

	<div  class="m-3 font-fuent-black text-justify">

		<section class="mostrar">

		<p v-html="noticia.texto"></p>
		</div>

		<dt>
			<div v-if="noticia.doc_name">
			<a :href="'public/noticias/show_document/'+noticia.id+'/'+noticia.doc_name" target="_blank">
				<img :src="icono" width="40px" height="40" class="mr-2 font-link">
				{{ noticia.doc_name }}
			</a>
		</div>
		</dt>
	</div>

</div>

<script>

	var Bus = new Vue()

	var notice = new Vue({
		el:"#notice",
		data:{
			noticia: '',
			extension: '',
			icono: ''
		},
		methods:{
			set_noticia(noticia)
			{
				this.noticia = noticia
				this.get_extension(this.noticia.doc_name)
			},
			get_extension(ruta)
			{
				var path = ruta;
				var path_splitted = path.split('.');
				this.extension = path_splitted.pop();

				switch (this.extension) {
					case 'pdf':
						this.icono = 'img/descargar/pdf.png'
						break;
					case 'doc':
						this.icono = 'img/descargar/word.svg'
						break;
					case 'docx':
						this.icono = 'img/descargar/word.svg'
						break;
					case 'xlsx':
						this.icono = 'img/descargar/excel.svg'
						break;
					case 'rar':
						this.icono = 'img/descargar/rar.svg'
						break;	
					case 'zip':
						this.icono = 'img/descargar/rar.svg'
						break;				
					default:
						this.icono = 'img/descargar/generico.svg'
						break;
				}
			}

		},
		filters:{
            // convierte fecha a formato dd/mm/yyyy
            DDMMYYYY: function(value){
                               
                var ano = value.substr(0,4);
                var mes = value.substr(5,2);
                var dia = value.substr(8,2);
                return dia+'/'+mes+'/'+ano;  
            }
        },
		created(){
			var self = this
			Bus.$on('get_noticia',function(noticia){
				self.set_noticia(noticia)
			});

			Bus.$on('send_noticia', function(noticia){
				self.set_noticia(noticia)
			})
		}
	})

	
$(function() {

	$('section.mostrar').expander({
		slicePoint: 500, // si eliminamos por defecto es 100 caracteres
		expandText: 'Leer mas...', // por defecto es 'read more...'
		expandPrefix: '&nbsp;', //valor de espacio por defecto es '&hellip; ',
		collapseTimer: 0, // tiempo de para cerrar la expanción si desea poner 0 pra no cerrar
		userCollapseText: 'ocultar' // por defecto es 'read less...'
		});
	});
</script>
     <?php /**PATH /Users/pablo/Documents/misproyectos/freelance/asodir/proyectoasodir/resources/views/public/publicaciones/show.blade.php ENDPATH**/ ?>
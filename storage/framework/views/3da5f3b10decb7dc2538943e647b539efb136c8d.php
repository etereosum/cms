<div id="imagen" class="carousel slide ml-2" data-ride="carousel">
  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#imagen" data-slide-to="0"></li>
    <li data-target="#imagen" data-slide-to="1"></li>
    <li data-target="#imagen" data-slide-to="2"></li>
    <li data-target="#imagen" data-slide-to="3"></li>
    <li data-target="#imagen" data-slide-to="4"></li>
  </ul>
  
  <!-- The slideshow -->
  <div class="carousel-inner">
    <?php $__currentLoopData = $data['banner']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $elemento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      
      <?php if($elemento->posicion == 1): ?>
        <div class="carousel-item active">
          <img src="<?php echo e(asset($elemento->ruta)); ?>">
        </div>
      <?php else: ?>
        <div class="carousel-item">
          <img src="<?php echo e(asset($elemento->ruta)); ?>" width="100%" >
        </div>
      <?php endif; ?>
       
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
  
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#imagen" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#imagen" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div><?php /**PATH /Users/pablo/Documents/misproyectos/freelance/asodir/proyectoasodir/resources/views/public/carrusel.blade.php ENDPATH**/ ?>
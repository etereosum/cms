<body style="margin:0px 0px 0px 0px;background:#fffff0;">
	<?php echo $__env->make('librerias.libreria', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

	<!--header-->
	<?php echo $__env->make('public.template.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

	<!--navbar-->
	<?php echo $__env->make('public.template.navbar_propia', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

	<!--menu_banner-->
	<?php echo $__env->make('public.template.menu_banner', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

	<!--publicaciones-->
	<?php echo $__env->make('public.template.publicaciones', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

	<!--link-->
	<?php echo $__env->make('public.template.link', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

	<!--footer-->
	<?php echo $__env->make('public.template.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	
</body>




<?php /**PATH /Users/pablo/Documents/misproyectos/freelance/asodir/proyectoasodir/resources/views/index.blade.php ENDPATH**/ ?>
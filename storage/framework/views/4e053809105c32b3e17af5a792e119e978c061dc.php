<!--Link-->

  
<div class="row" >
	<div class="col-md-8 offset-md-2" style="margin-top:40px;">
		
		<div class="autoplay">

			<?php $__currentLoopData = $data['links']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

				<a href="<?php echo $link->link; ?>" target="_blank" style="width:400px;height:120px;;">
					<img src="<?php echo e(asset($link->ruta)); ?>" style="width:100%;height:100%;padding: 0px 20px 0px 20px;">
				</a>

			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		</div>
	</div>
</div>


<script type="text/javascript" src="<?php echo e(asset('js/jquery-migrate.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('slick/slick.min.js')); ?>"></script>

<script type="text/javascript">
	$('.autoplay').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000
	});
</script>

		    
<?php /**PATH /Users/pablo/Documents/misproyectos/freelance/asodir/proyectoasodir/resources/views/public/template/link.blade.php ENDPATH**/ ?>
<!--publicaciones-->
<div class="container-fluid ">
  
        <div class="row">
          <div class="col-md-10 offset-md-1 ">
            <div class="row">
                <div class="col-md-5 pt-4">
                  <div class="card my-2" ><!--Inicio Card  Menú Principal -->
                    <span class="card-header font-panel-titulo"style="background:#e3342f">Publicaciones</span>        
                    <div class="card-body ml-3 mr-3 font-panel-texto">
                      <?php echo $__env->make('public.publicaciones.list', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>  
                    </div>
                  </div>
                </div>  
          
                <div class="col-md-7 ">
                  <?php echo $__env->make('public.publicaciones.show', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>  
            </div>
        </div>
      </div>
  </div><?php /**PATH /Users/pablo/Documents/misproyectos/freelance/asodir/proyectoasodir/resources/views/public/template/publicaciones.blade.php ENDPATH**/ ?>
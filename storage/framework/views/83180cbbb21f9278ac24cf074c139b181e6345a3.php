  <div class="card "><!--Inicio Card  Menú Principal -->
    <span class="card-header font-panel-titulo"style="background:#e3342f;" >Menú Principal</span>
    <div class="card-body font-panel-texto ">
      <ul class="list-unstyled mb-0  ">
        <li class="d-flex">
            <a href="<?php echo e(route('junta.public.index')); ?>"><img src="<?php echo e(asset('/img/menu_principal/junta.svg')); ?>" 
              class="m-1" width="50px" height="50px">Directorio Junta Diréctiva</a>
        </li>
        <li  class="d-flex">
            <a href="<?php echo e(route('plan_accion.index')); ?>"><img src="<?php echo e(asset('/img/menu_principal/plan.svg')); ?>" 
              class="m-1" width="50px" height="50px">Plan de Accíon</a>
        </li>
        <li  class="d-flex">
            <a href="<?php echo e(route('sgsst.index')); ?>"><img src="<?php echo e(asset('/img/menu_principal/sgsst.svg')); ?>" 
              class="m-1" width="50px" height="50px">S.G.S.S.T</a>
        </li>
        <li class="d-flex">
          <a href="#"><img src="<?php echo e(asset('/img/menu_principal/canal.svg')); ?>" 
            class="m-1" width="50px" height="50px">Canal Youtube</a>
        </li>
      </ul>   
    </div>

    
  </div>

  <?php echo $__env->yieldContent('mapa'); ?>    <?php /**PATH /Users/pablo/Documents/misproyectos/freelance/asodir/proyectoasodir/resources/views/public/template/menu_banner_principal.blade.php ENDPATH**/ ?>
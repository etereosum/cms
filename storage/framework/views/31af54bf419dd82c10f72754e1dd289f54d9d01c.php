  <div id="list">
    <ul class="list-unstyled mb-0">

        <li v-for="(noticia,index) in noticias"> 
          <div class="card border-0">
            <a href="#notice" style="font-size: 0.9rem;" 
              @click="get_noticia(index)">{{ noticia.titulo }}</a>
          </div>
            <span class="text-muted" style="font-size: 13.5px; font-family: Verdana;">
              Publicado: {{ noticia.created_at | DDMMYYYY}}</span><p>

        </li>

        <br>

        <nav aria-label="Page navigation example">
          <ul class="pagination pagination-sm">
            <li class="page-item"  v-if="pagination.current_page > 1">
              <a class="page-link" href="#" aria-label="Previous"
                @click.prevent="change_page(pagination.current_page - 1)">
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>

            <li class="page-item"
              v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '' ]">
              <a class="page-link" href="#" @click.prevent="change_page(page)">
                {{ page }}</a>
            </li>

            <li class="page-item" v-if="pagination.current_page < pagination.last_page">
              <a class="page-link" href="#" aria-label="Next"
                @click.prevent="change_page(pagination.current_page + 1 )">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
          </ul>
        </nav> 

    </ul> 

  </div>

  <script>
    var list = new Vue({
      el:"#list",
      data:{
        noticia_id: "<?php echo e($noticia_id); ?>",
        noticias:'',
        pagination : {
          total        : 0,
          current_page : 0,
          per_page     : 0,
          last_page    : 0,
          from         : 0,
          total        : 0,
          to           : 0
        },
        offset : 2
      },
      methods:{
        get_noticia(index)
        {
          var self = this
          Bus.$emit('send_noticia',self.noticias[index])
        },
        get_list_noticias(page)
        {
          var self = this
          var route = "/public/home/noticias/list?page=" + page
          console.log(route)
          axios.get(route)
            .then(function(res){
              console.log(res)
              
              if(!res.data.error){
                console.log('data',res.data.dat.data)
                
                self.noticias = res.data.dat.data
                self.pagination = res.data.pagination
                Bus.$emit('get_noticia',res.data.dat.data[0])
                
              }              
            })
        },
        change_page(page){
            this.pagination.current_page = page
            this.get_list_noticias(page)
        }
      },
      computed:{
        isActived: function(){
            return this.pagination.current_page
        },
        pagesNumber: function(){
            if(!this.pagination.to) {
                return [];
            }

            var from = this.pagination.current_page - this.offset
            if(from < 1) { from = 1 }

            var to = from + (this.offset * 2)
            if( to >= this.pagination.last_page ) {
                to = this.pagination.last_page
            }

            var pagesArray = [];
            while(from <= to){
                pagesArray.push(from)
                from++
            }

            return pagesArray
        }
      },
      filters:{
            // convierte fecha a formato dd/mm/yyyy
            DDMMYYYY: function(value){
                               
                var ano = value.substr(0,4);
                var mes = value.substr(5,2);
                var dia = value.substr(8,2);
                return dia+'/'+mes+'/'+ano;  
            }
      },
      created(){
        this.get_list_noticias()
      }
    })
  </script><?php /**PATH /Users/pablo/Documents/misproyectos/freelance/asodir/proyectoasodir/resources/views/public/publicaciones/list.blade.php ENDPATH**/ ?>
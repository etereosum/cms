<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Elemento extends Model
{
    protected $fillable = [
        'nombre', 
        'estado',
        'posicion', // se utiliza cuando son varios elementos 
        'ruta',// ruta donde se guarda el archivo
        'tipo', // Puede ser Facebook, SGSST
        'link' // enlace a otro sitio web
    ];

}

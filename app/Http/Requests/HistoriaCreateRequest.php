<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class HistoriaCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'img_file'  => 'image',
            'titulo'    => 'required|string',
            'texto'     => 'required|string'

        ];
    }

    public function messages()
    {
        return [
            'img_file.image'  => 'La :attribute no tiene el formato requerido',
            'titulo.required' => 'El :attribute es requerido',
            'titulo.string'   => 'El :attribute debe ser texto',
            'texto.required'  => 'El :attribute es requerido',
            'texto.string'    => 'El :attribute debe ser texto'
        ];
    }

    public function attributes()
    {
        return [
            'img_file' => 'imagen',
            'titulo'   => 'Titulo',
            'texto'    => 'contenido de la historia'
        ];
    }
}

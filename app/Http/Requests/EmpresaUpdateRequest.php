<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'        => 'required',
            'nit'           => 'required',
            'telefono_1'    => 'required',
            'celular_1'     => 'required',
            'direccion'     => 'required',
            'email'         => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'telefono_1' => 'telefono uno',
            'celular_1'  => 'celular uno'
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JuntaCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres'   => 'required|string',
            'apellidos' => 'required|string',
            'cargo_id'  => 'required',
            'position'  => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'cargo_id' => 'cargo'
        ];
    }    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\QuienesSomosTrait;
use App\Pos;


class HorizonteController extends Controller
{
    Use QuienesSomosTrait;
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'texto_horizonte'    => 'required|string'
        ]);
        
        $this->get_horizonte();
        $this->horizonte->fill($request->all());
        $this->horizonte->texto = $request->texto_horizonte;

        if ( ! $this->horizonte->isDirty() ) {
            
            session()->flash('info','Ningun cambio en registro');
            return redirect()->route('quienes_somos.index');
            
        }

        $this->horizonte->user_update_id  = 1;
        $this->horizonte->save();

        session()->flash('success','Horizonte institucional editado exitosomente !!!');
        return redirect()->route('quienes_somos.index');
    }
}

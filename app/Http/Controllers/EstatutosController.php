<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\QuienesSomosTrait;
use App\Traits\DocTrait;
use App\Traits\ImgTrait;
use App\Pos;

class EstatutosController extends Controller
{
    use QuienesSomosTrait;
    use DocTrait;
    use ImgTrait;
    protected $estatutos;

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'img_file'           => 'image',
            'texto_estatutos'    => 'required|string'
        ]);
        
        $this->getEstatutosQst();
        $this->estatutos->fill($request->all());
        $this->estatutos->texto = $request->texto_estatutos;

        if ( $request->hasFile('img_file') ){

            $this->delete_img($this->estatutos->img_ruta);
            $data = $this->save_img($request, 'img/estatutos');
            $this->estatutos->img_name = $data['img_name'];
            $this->estatutos->img_ruta = $data['img_ruta'];
        }


        if ( $request->hasFile('doc_file') ){

            $this->delete_doc($this->estatutos->doc_ruta);
            $doc = $this->save_doc($request,'estatutos');
            $this->estatutos->doc_name = $doc['nombre'];
            $this->estatutos->doc_ruta = $doc['ruta'];
        }

        if ( ! $this->estatutos->isDirty() ) {
            
            session()->flash('info','Ningun cambio en registro');
            return redirect()->route('quienes_somos.index');      
        }

        $this->estatutos->user_update_id  = 1;
        $this->estatutos->save();

        session()->flash('success','Estatutos editado exitosomente !!!');
        return redirect()->route('quienes_somos.index');
    }

    public function delete_doc($obj_id)
    {
        $pos = Pos::find($obj_id);
        if($pos){
            $this->delete_doc($pos->doc_ruta);
            $pos->doc_name = null;
            $pos->doc_ruta = null;
            $pos->save();
        }

        return redirect()->route('quienes_somos.index');    
    }

    public function delete_image()
    {
        $this->getEstatutosQst();
        $this->delete_img($this->estatutos->img_ruta);
        $this->estatutos->img_ruta = null;
        $this->estatutos->img_name = null;
        $this->estatutos->save();

        session()->flash('success','Se eliminó la imagen !!!');
        return redirect()->route('quienes_somos.index');
    }

}

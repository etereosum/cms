<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pos;
use App\Traits\HomeTrait;
use App\Elemento;
use App\Empresa;

class SgsstController extends Controller
{

	use HomeTrait;
	protected $empresa;
	protected $links;

    public function index()
    {
        $sgsst = Pos::where('tipo_pos_id',7)->get();

        return view('public.sgsst.index')
            ->with('sgsst',$sgsst)
            ->with('data',getDataIniHp());
    }
}

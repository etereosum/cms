<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ElementTrait;
use App\Elemento;
use DB;

class LinkController extends Controller
{

    use ElementTrait;

    protected $element;

    public function __construct()
    {
        $this->element = new Elemento();
    }

    public function index()
    {
        $imagenes       = Elemento::where('tipo','Link')->orderBy('posicion')->get();
        $num_elements   = Elemento::where('estado','Activo')->count();
        $ruta_view      = rutas_view('home');

        return view('admin.links.index')
            ->with('imagenes',$imagenes)
            ->with('num_elements',$num_elements)
            ->with('ruta_view',$ruta_view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->element->fill($request->all());
        $this->element->tipo = 'Link';

        $this->mover_posicion($request->posicion,'Link');
        
        if ( $request->hasFile('img_file') ){
            $this->save_element($request, 'img/links');
        }

        return redirect()->route('links.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->element  = Elemento::find($id);
        $num_elements   = Elemento::where('estado','Activo')->count();
        $ruta_view      = rutas_view('home');

        $res = [
            'error' => false,
            'dat'   => [
                'element'       => $this->element,
                'num_elements'  => $num_elements,
                'ruta_view'     => $ruta_view
            ]
        ];

        return response()->json($res);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function actualizar(Request $request)
    {
        $this->element =  Elemento::find($request->id);
        $this->element->fill($request->all());

        if ( $request->hasFile('img_file') ){
            $this->delete_element($this->element->ruta);
            $this->save_element($request, 'img/links');
        }

        // if ( ! $this->element->isDirty() ) {
          
        //     session()->flash('info','Ningun cambio en registro');    
        //     return redirect()->route('links.index');

        // }
        $this->mover_posicion($request->posicion,'Link');
        $this->element->save();

        session()->flash('success','Se editó el registro exitosamente !!!');   
        return redirect()->route('links.index');        
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $elemento = Elemento::find($id);
            $this->devolver_posicion($elemento->posicion, 'Link');
            $this->delete_element($elemento->ruta);
            $elemento->delete();
    
            DB::commit();

            session()->flash('success','Se eliminó el registro exitosamente !!!');
            return redirect()->route('links.index');
        
        } catch (\Throwable $th) {
            DB::rollback();
            session()->flash('danger','Ocurrio un error al borrar !!!');
            return redirect()->route('links.index');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\HomeTrait;
use App\Traits\DocTrait;
use App\Pos;


class PlanAccionController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth",["except" => ["index"]]);
    }
    public function index()
    {
        $plan = Pos::where('tipo_pos_id',6)->get();

        return view('public.plan_accion.index')
            ->with('plan',$plan)
            ->with('data',getDataIniHp());
    }

    
}

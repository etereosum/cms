<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\QuienesSomosTrait;
use App\Pos;

class QuienesSomosController extends Controller
{

    use QuienesSomosTrait;

    protected $historia;
    protected $estatutos;
    protected $horizonte;

    public function __construct()
    {
        $this->middleware('auth',['except' => ['index_public']]);
        $this->getHistoriaQst();
        $this->getEstatutosQst();
        $this->getHorizonteQst();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.quienes_somos.index')
            ->with('historia',$this->historia)
            ->with('estatutos',$this->estatutos)
            ->with('horizonte',$this->horizonte)
            ->with('ruta_view', rutas_view('quienes somos'));
    }


    public function index_public()
    {
        return view('public.resena_historica.index')
            ->with('historia',$this->historia)
            ->with('pos',$this->estatutos)
            ->with('horizonte',$this->horizonte)            
    		->with( 'data',getDataIniHp() );
    }

    public function create(){}

    public function store(Request $request){}

    public function show($id){}

    public function edit($id){}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $type)
    {
        //
    }

    public function destroy($id){}
}

<?php

namespace App\Http\Controllers;
use App\Institucion;
use App\Municipio;
use App\Depto;
use Validator;
use Auth;

use Illuminate\Http\Request;

class InstitucionController extends Controller
{
    public function create()
    {
        $deptos = Depto::orderBy('nombre','desc')->get();
        $municipios = Municipio::orderBy('nombre','desc')->get();

        return ['deptos' => $deptos, 'municipios' => $municipios];
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre'        => 'required|string|unique:instituciones',
            'direccion'     => 'required|string',
            'municipio_id'  => 'required',
            'telefono'      => 'required',
            'email'         => 'required|email'
        ]);

        if($validator->fails()){
            return $validator->errors();
        }

        $institucion = new Institucion();
        $institucion->fill($request->all());
        $institucion->user_create_id = Auth::user()->id;
        $institucion->save();
        
        return response()->json($institucion);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;

use App\Cargo;
use App\Depto;
use App\User;
use App\Rol;
use Auth;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('rol','<>','Junta directiva')
                ->orderBy('updated_at','desc')
                ->get();

        return view('admin.users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = getEnumValues('users', 'estado');

        return view('admin.users.create')
            ->with('estados',$estados);
            
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->user_create_id = 1;
        $user->password = bcrypt($request->password);
        $user->save();

        session()->flash('success','Registro creado exitosamente !!!');
        return redirect()->route('users.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estados = getEnumValues('users', 'estado');
        $user    = User::find($id);

        return view('admin.users.edit')
            ->with('estados',$estados)
            ->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user = User::find($id);
        $user->fill( $request->all() );

        if(!$user->isDirty()){
            session()->flash('info','Ningun cambio en registro');    
            return redirect()->route('users.edit', $user->id);
        }
        $user->user_update_id = 1;
        $user->save();

        session()->flash('success','Registro editado exitosamente !!!'); 
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return 'delete';
    }

    public function edit_pass($user_id)
    {
        return view('admin.users.edit_pass')
            ->with('id',$user_id);
    }

    public function update_pass(Request $request)
    {
        $this->validate($request,[
            'password'     => 'required|min:6',
            'confirm'      => 'required|same:password'
        ],[
            'password.required' => 'Se requiere la nueva contraseña',
            'password.min'      => 'La contraseña debe tener por lo menos 6 caracteres',
            'confirm'           => 'Se requiere la confirmación de la nueva contraseña',
            'confirm.same'      => 'Las contraseñas no coinciden'
            ]);

        try {
            $user = User::find($request->user_id);
            $user->password = bcrypt($request->password);
            $user->user_update_id = Auth::user()->id;
            $user->save();

            session()->flash('success','Se editó la contraseña');
            return redirect()->route('users.index');

        } catch (\Exception $e) {
            session()->flash('danger',$e->getMessage());
            return redirect()->route('users.edit_pass',$user->id);
        } 

            


    }
}

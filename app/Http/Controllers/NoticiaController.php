<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoticiaCreateRequest;
use App\Http\Requests\NoticiaUpdateRequest;
use Illuminate\Support\Facades\DB;
use App\Traits\NoticiaTrait;
use Illuminate\Http\Request;
use App\Mail\SendNoticia;
use App\Traits\HomeTrait;
use App\Traits\DocTrait;
use App\Noticia;
use App\Empresa;
use App\Correo;
use App\Pos;

use Storage;
use Flash;
use Mail;

//use App\Traits\HomeTrait;

class NoticiaController extends Controller
{
    use NoticiaTrait;
    use HomeTrait;
    use DocTrait;

    protected $noticia;
    protected $noticias;
	protected $banner;
	protected $empresa;
	protected $links;

    public  function __construct()
    {    
        $this->noticia = new Pos();
        $this->noticia->tipo_pos_id = 1; //noticia
        $this->middleware('auth',['except' => ['post_email']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Pos::orderBy('updated_at','desc')
            ->where('tipo_pos_id',1)
            ->get();

        return view('admin.noticias.index')
            ->with('noticias', $noticias)
            ->with('info',null)
            ->with('ruta_view',rutas_view('home'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.noticias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NoticiaCreateRequest $request)
    {
        $this->noticia->titulo   = $request->titulo;
        $this->noticia->texto    = $request->texto;
        $this->noticia->user_create_id  = 1;
        
        if ( $request->hasFile('img_file') ){
            $data = $this->save_img($request);
        }

        if ( $request->hasFile('doc_file') ){
            $doc = $this->save_doc($request, 'noticias');
            $this->noticia->doc_name = $doc['nombre'];
            $this->noticia->doc_ruta = $doc['ruta'];
        }

        $this->noticia->save();

        session()->flash('success','Se creó la noticia exitosamente !!!');

        return redirect()->route('noticias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function show($noticia_id)
    {    
        $noticia = Pos::find($noticia_id);
        return view('admin.noticias.show')
            ->with('noticia',$noticia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function edit($noticia_id)
    {
        $noticia = Pos::find($noticia_id);
        
        return view('admin.noticias.edit')
            ->with('noticia',$noticia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function update(NoticiaUpdateRequest $request,$noticia_id)
    {
        $this->noticia = Pos::find($noticia_id);
        $this->noticia->fill($request->all());

        if ( $request->hasFile('img_file') ){
            $this->delete_img();
            $this->save_img($request);
        }

        if ( $request->hasFile('doc_file') ){

            $this->delete_doc($this->noticia->doc_ruta);
            $doc = $this->save_doc($request, 'noticias');
            $this->noticia->doc_name = $doc['nombre'];
            $this->noticia->doc_ruta = $doc['ruta'];
        }

        if ( ! $this->noticia->isDirty() ) {

            session()->flash('info','Ningun cambio en registro');    
            return redirect()->route('noticias.edit',$noticia_id);
        }

        $this->noticia->user_update_id  = 1;

        $this->noticia->save();

        session()->flash('success','Se editó la noticia exitosamente !!!');
        return redirect()->route('noticias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function destroy($noticia_id)
    {
        $this->pos = Pos::find($noticia_id);
        if ( $this->pos->img_ruta ){
            $this->delete_img();
        }

        if ( $this->pos->doc_ruta ){
            $this->delete_doc($this->pos->doc_ruta);
        }
        $this->pos->delete();

        session()->flash('success','Se eliminó la noticia exitosamente !!!');
        return redirect()->route('noticias.index');
    }

    public function send($noticia_id)
    {
        $noticia = Pos::find($noticia_id);
        $noticia->texto = contador_palabras($noticia->texto,40);

        $mails = Correo::all()->toArray();
        $collection = collect($mails);
        $collection_emails = $collection->pluck('correo');

        Mail::to($collection_emails)->send(
            new SendNoticia($noticia)
        );

        session()->flash('success','Mensaje enviado !!!');
        return redirect()->route('noticias.index');
    }

    public function post_email($pos_id)
    {
        $pos = hp_get_noticia($pos_id);

        return view('public.publicaciones.post_email')
            ->with('pos',$pos)
            ->with('data',getDataIniHp());
    }
    
    public function delete_doc($obj_id)
    {
        $pos = Pos::find($obj_id);
        if($pos){
            $this->delete_doc($pos->doc_ruta);
            $pos->doc_name = '';
            $pos->doc_ruta = '';
            $pos->save();
        }
        return redirect()->route('noticias.edit',$obj_id);    
    }

    public function delete_image($noticia_id)
    {
        $noticia = Pos::find($noticia_id);
        $this->delete_img($noticia->img_ruta);
        $noticia->img_ruta = null;
        $noticia->img_name = null;
        $noticia->save();

        session()->flash('success','Se eliminó la imagen !!!');
        return redirect()->route('noticias.edit',$noticia->id);
    }

}

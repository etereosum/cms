<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
    use App\Traits\HomeTrait;
use App\Mail\SendEmail;
use Mail;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('public.contactos.contactanos')
            ->with('data',getDataIniHp());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function send_message(Request $request)
    {
        $this->validate($request,[
            'asunto'    => 'required',
            'nombre'    => 'required',
            'email'     => 'email|required',
            'mensaje'   => 'required'
        ]);
        
        Mail::to("contacto@asodir.org")->send(
            new SendEmail($request->nombre, 
                          $request->telefono, 
                          $request->email,
                          $request->asunto,
                          $request->mensaje)
        );

        session()->flash('success','Mensaje enviado !!!, pronto estaremos respondiendo');
        return redirect()->route('contactos.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Pos;
use DB;

class SearchController extends Controller
{
    protected $string;

    public function get($string = null)
	{
        $html = '';
        $this->string = $string;

        if(strlen($string) == 0){
            return $this->sin_resultados();            
        } else {

            $posts = DB::table('pos')
                ->join('tipo_pos','pos.tipo_pos_id','=','tipo_pos.id')
                ->where(function($query){
                    $query->where('pos.titulo','like','%'.$this->string.'%')
                          ->orWhere('pos.texto','like','%'.$this->string.'%');
                })
                ->select('pos.id as id',
                         'pos.estado as estado',
                         'pos.img_name as img_name',
                         'pos.img_ruta as img_ruta',
                         'pos.texto as texto',
                         'pos.titulo as titulo',
                         'pos.doc_name as doc_name',
                         'pos.doc_ruta as doc_ruta',
                         'tipo_pos.nombre as tipo',
                         'pos.created_at as created_at')
                ->simplePaginate(5);

            $num_posts = DB::table('pos')
                ->join('tipo_pos','pos.tipo_pos_id','=','tipo_pos.id')
                ->where(function($query){
                    $query->where('pos.titulo','like','%'.$this->string.'%')
                          ->orWhere('pos.texto','like','%'.$this->string.'%');
                })
                ->count();
                
            if(count($posts)){
                $html .= "<p style='margin-left: 40px;'>{$num_posts} datos encontrados . . .</p>";
                foreach($posts as $pos)
                {
                    $route = route('search.show',$pos->id);
                    $date = Carbon::create($pos->created_at);
                    $texto = contador_palabras($pos->texto, 20);
                    $html .= "
                        <ul style='list-style: none;''>
                            <li>
                                <a href='{$route}' style='font-size:17px;font-weight:bold;text-decoration:underline;list-style:none;'>
                                    {$pos->titulo}</a>
                                <p style='font-size:12px;'>
                                    {$date->format('d-m-Y')} 
                                <br>
                                    ({$pos->tipo}) {$texto } . . . . .  . . 
                                </p>
                            </li>
                        </ul>
                    ";
                }

                $html .= "<div style='margin-left: 40px;'>{$posts->links()}</div>";
            } 
            else {
                return $this->sin_resultados();
            }
    
            return view('public.search.index')
                ->with('data',getDataIniHp())
                ->with('html',$html);

        }//.else
    }
    
    public function show($pos_id)
    {
        $pos = hp_get_noticia($pos_id);

        return view('public.publicaciones.post_email')
            ->with('pos',$pos)
            ->with('data',getDataIniHp());
    }

    public function sin_resultados()
    {
        $html = "
                <br><br><br><br>
                <center>
                <img src=".asset('/imagenes/hombre.png')." width=50
                 style='display:inline;'>
                </img>
                <h5 style='display:inline;margin-left:30px;align:left;'>No se encontraron resultados</h5>
                </center>
                ";

            return view('public.search.index')
                ->with('data',getDataIniHp())
                ->with('html',$html);
    }


}

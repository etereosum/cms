<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\JuntaUpdateRequest;
use App\Http\Requests\JuntaCreateRequest;
use App\Traits\JuntaTrait;
use App\Cargo;
use App\User;
use Auth;
use DB;

class JuntaController extends Controller
{
    use JuntaTrait;
    protected $junta;
    protected $integrante;
    protected $links;
    protected $empresa;


    public function __construct()
    {
        $this->integrante = new User();
        $this->middleware('auth', ["except" => ["junta_public"]]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->junta = User::where('rol','Junta directiva')
            //->where('estado','Activo')
            ->orderBy('posicion','asc')
            ->get();

        return view('admin.junta.index')
            ->with('junta',$this->junta);
    }

    public function junta_public()
    {
        $this->junta = User::where('rol','Junta directiva')
            ->where('estado','Activo')
            ->orderBy('posicion','asc')
            ->get();

        return view('public.junta.index')
            ->with('junta',$this->junta)
            ->with('data',getDataIniHp());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cargos = Cargo::orderBy('nombre')->get();
        $roles  = getEnumValues('users','rol');
        $integrantes = User::where('estado','Activo')
            ->where('rol','Junta directiva')
            ->orderBy('posicion','asc')
            ->get();

        return view('admin.junta.create')
            ->with('cargos',$cargos)
            ->with('roles',$roles)
            ->with('integrantes',$integrantes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JuntaCreateRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->agregarPosicionJt($request->position);

            $this->integrante->fill($request->all()); 
    
            $this->integrante->rol = 'Junta directiva';
            $this->integrante->user_create_id = Auth::user()->id;
            $this->integrante->save();
    
            DB::commit();

            session()->flash('success','Se creó el registro exitosamente !!!');
            return redirect()->route('junta.index');
        }
        catch(\Exception $e) {
            DB::rollback();

            session()->flash('danger','Se presento un error =(');
            return redirect()->route('junta.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($integrante_id)
    {
        $this->integrante = User::find($integrante_id);

        return view('admin.junta.show')
            ->with('integrante',$this->integrante);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $integrante = User::find($id);
        $cargos     = Cargo::orderBy('nombre')->get();
        $roles      = getEnumValues('users','rol');

        $integrantes = User::where('estado','Activo')
            ->where('rol','Junta directiva')
            ->orderBy('posicion','asc')
            ->get();

        return view('admin.junta.edit')
            ->with('integrantes',$integrantes)
            ->with('integrante',$integrante)
            ->with('cargos',$cargos)
            ->with('roles',$roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $integrante_id)
    {
        DB::beginTransaction();

        try {          
            $integrante = User::find($integrante_id);
            $bandera = 0;

            $this->integrante = User::find($integrante_id);
            $this->integrante->fill($request->all()); 

            if(isset($request->position) && $request->position != $integrante->posicion){
            
                $this->actualizarPosicionJt($request->position, $integrante_id);
                $bandera = 1;
            } 

            if ( ! $this->integrante->isDirty() && $bandera == 0) {
                session()->flash('info','Ningun cambio en registro');    
                return redirect()->route('junta.edit',$integrante_id);
            }
            $this->integrante->user_update_id = Auth::user()->id;
            $this->integrante->save();

            DB::commit();

            session()->flash('success','Se editó el registro exitosamente !!!');
            return redirect()->route('junta.index');
        } 
        catch (\Exception $e) {
            DB::rollback();
            session()->flash('danger','Error al actualizar !!! '.$e->getMessage());
            return redirect()->route('junta.edit',$integrante_id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($integrante_id)
    {
        DB::beginTransaction();
        try{
            $this->integrante = User::find($integrante_id);
            $this->eliminarPosicionJt($this->integrante->posicion);
            $this->integrante->delete();

            DB::commit();
    
            session()->flash('success','Se eliminó el registro exitosamente !!!');
            return redirect()->route('junta.index');        

        } catch (\Exception $e){
            DB::commit();
            session()->flash('error','Error al eliminar : '.$e->getMessage());
            return redirect()->route('junta.index');        
        }
    }


}

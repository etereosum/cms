<?php

namespace App\Http\Controllers;

use App\Http\Requests\HistoriaCreateRequest;
use App\Http\Requests\HistoriaUpdateRequest;
use Illuminate\Http\Request;
use App\Traits\ImgTrait;
use App\Traits\DocTrait;
use App\Traits\QuienesSomosTrait;
use App\Pos;
use Storage;
use Auth;

class HistoriaController extends Controller
{
    use QuienesSomosTrait;
    use ImgTrait;
    use DocTrait;
    protected $historia;

    public  function __construct()
    {
        $this->historia = new Pos();
        $this->historia->tipo_pos_id = 2; 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HistoriaCreateRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->historia = Pos::find($id);
        return $this->historia;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estados = getEnumValues('pos', 'estado');
        $this->historia = Pos::find($id);
        return ['estados' => $estados, 'historia' => $this->historia];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'img_file'          => 'image',
            'texto_historia'    => 'required|string'
        ],[
            'img_file.image' => 'Formato de imagen no reconocido',
            'texto_historia.required' => 'Se requiere el texto de la historia'
        ]);
        
        $this->historia = Pos::find($id);
        $this->historia->fill($request->all());
        $this->historia->texto = $request->texto_historia;
        
        if ( $request->hasFile('img_file') ){

            $this->delete_img($this->historia->img_ruta);
            $data = $this->save_img($request, 'img/historia');
            $this->historia->img_name = $data['img_name'];
            $this->historia->img_ruta = $data['img_ruta'];
        }

        if ( ! $this->historia->isDirty() ) {
            
            session()->flash('info','Ningun cambio en registro');
            return redirect()->route('quienes_somos.index');
            
        }

        $this->historia->user_update_id  = Auth::user()->id;
        $this->historia->save();

        session()->flash('success','Historia editada exitosomente !!!');
        return redirect()->route('quienes_somos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete_image()
    {
        $this->getHistoriaQst();
        $this->delete_img($this->historia->img_ruta);
        $this->historia->img_ruta = null;
        $this->historia->img_name = null;
        $this->historia->save();

        session()->flash('success','Se eliminó la imagen !!!');
        return redirect()->route('quienes_somos.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NormativaCreateRequest;
use App\Traits\NoticiaTrait;
use App\Traits\HomeTrait;
use App\Traits\DocTrait;
use App\Traits\ImgTrait;
use App\Empresa;
use App\Pos;
use Auth;

/** 
 * Clase que me permite gestionar las normativas
 * tanto en el panel administrativo como en el front
 */

class NormativaController extends Controller
{
    use ImgTrait;
    use DocTrait;
    use HomeTrait;

    protected $normativas;
    protected $normativa;
    protected $links;
    protected $empresa;
    protected $id_encabezado = 5; // id del pos encabezado vista normativas

    public  function __construct()
    {   
        $this->middleware('auth',["except" => ['index_public']]);
        $this->normativa = new Pos();
        $this->normativa->tipo_pos_id = 5; //normativa
    }

    /**
     * Listado de las normativas en el panel admin
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //los tipo_pos_id = 5 son para normativas

        $this->normativas = Pos::where('tipo_pos_id',5)
            ->where('id', '<>',$this->id_encabezado)
            ->orderBy('updated_at','desc')
            ->simplePaginate(5);

        return view('admin.normativa.index')
            ->with('normativas', $this->normativas);
    }
    
    /**
     * Listado de las normativas en el front
     * 
     * @return \Illuminate\Http\Response
     */

    public function index_public()
    {
        //los tipo_pos_id = 5 son para normativas

        $normativas = Pos::where('tipo_pos_id',5)
            ->where('id', '<>',$this->id_encabezado)
            ->orderBy('created_at','desc')
            ->simplePaginate(10);

        $encabezado  = Pos::find($this->id_encabezado);

        return view('public.normativa.index')
            ->with('normativas', $normativas)
            ->with('data',getDataIniHp())
            ->with('encabezado',$encabezado);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.normativa.create');
    }

    /**
     * Permite crear normativas 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NormativaCreateRequest $request)
    {
        $this->normativa->fill($request->all());

        if($request->created_at)
            $this->normativa->created_at = $request->created_at;

        $doc = $this->save_doc($request, 'normativa');

        $this->normativa->doc_name = $doc['nombre'];
        $this->normativa->doc_ruta = $doc['ruta'];
        $this->normativa->user_create_id = Auth::user()->id;
        $this->normativa->save();

        session()->flash('success','Se cre車 el registro exitosamente !!!');

        return redirect()->route('normativa.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($normativa_id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($normativa_id)
    {
        $normativa = Pos::find($normativa_id);
        
        return view('admin.normativa.edit')
            ->with('normativa',$normativa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $normativa_id)
    {
        $this->normativa = Pos::find($normativa_id);

        $this->normativa->fill($request->all());

        if($request->created_at)
            $this->normativa->created_at = $request->created_at;

        if ( $request->hasFile('doc_file') ){
            
            if($this->normativa->doc_ruta)
                $this->delete_doc($this->normativa->doc_ruta);
            
            $doc = $this->save_doc($request, 'normativa');

            $this->normativa->doc_name = $doc['nombre'];
            $this->normativa->doc_ruta = $doc['ruta'];
        }

        if ( ! $this->normativa->isDirty() ) {
          
            session()->flash('info','Ningun cambio en registro');    
            return redirect()->route('normativa.edit', $normativa_id);
        }

        $this->normativa->user_update_id = Auth::user()->id;
        $this->normativa->save();

        session()->flash('success','Se edito el registro exitosamente !!!');
        return redirect()->route('normativa.index');
    }

    /**
     * Permite ver la vista de edici車n del encabezado de las
     * normativas
     */

    public function editEncabezado()
    {
        $encabezado = Pos::find($this->id_encabezado);

        return view('admin.normativa.encabezado.edit')
            ->with('normativa',$encabezado);
    }

    public function updateEncabezado(Request $request)
    {
        $this->normativa = Pos::find($this->id_encabezado);

        $this->normativa->fill($request->all());

        if($request->created_at)
            $this->normativa->created_at = $request->created_at;

        if ( $request->hasFile('img_file') ){

            $this->delete_img($this->normativa->img_ruta);
            $data = $this->save_img($request, 'normativas');
            $this->normativa->img_name = $data['img_name'];
            $this->normativa->img_ruta = $data['img_ruta'];

        }

        if ( ! $this->normativa->isDirty() ) {   
            session()->flash('info','Ningun cambio en registro');    
            return redirect()->route('normativa.edit', $normativa_id);
        }

        $this->normativa->user_update_id = Auth::user()->id;
        $this->normativa->save();

        session()->flash('success','Se edito el registro exitosamente !!!');
        return redirect()->route('normativa.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($normativa_id)
    {
        $normativa = Pos::find($normativa_id);
        $this->delete_doc($normativa->doc_ruta);
        $normativa->delete();

        session()->flash('success','Se elimino el registro exitosamente !!!');
        return redirect()->route('normativa.index');
    }

    public function get_documento($normativa_id)
    {
        $normativa = Pos::find($normativa_id);
        return $this->get_document($normativa);
    }
    

    public function delete_doc($obj_id)
    {
        $pos = Pos::find($obj_id);
        if($pos){
            $this->delete_doc($pos->doc_ruta);
            $pos->doc_name = '';
            $pos->doc_ruta = '';
            $pos->save();
        }

        return redirect()->route('normativa.edit',$obj_id);    
    }

    public function delete_image($normativa_id)
    {
        $normativa = Pos::find($normativa_id);
        $this->delete_img($normativa->img_ruta);
        $normativa->img_ruta = null;
        $normativa->img_name = null;
        $normativa->save();
        
        if ($normativa_id == $this->id_encabezado) {
            session()->flash('success','Se elimino la imagen !!!');
            return redirect()->route('normativa.edit_encabezado',$normativa->id);
        }

        session()->flash('success','Se elimino la imagen !!!');
        return redirect()->route('normativa.edit',$normativa->id);
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\BannerCreateRequest;
use Illuminate\Http\Request;
use App\Traits\ElementTrait;
use App\Elemento;
use Storage;
use DB;

class BannerController extends Controller
{
    use ElementTrait;
    protected $element;

    public function __construct()
    {
        $this->element = new Elemento();
        
    }

    public function index()
    {
        $imagenes       = Elemento::where('tipo','Banner')->orderBy('posicion')->get();
        $num_elements   = Elemento::where('estado','Activo')->count();

        return view('admin.banner.index')
            ->with('imagenes',$imagenes)
            ->with('num_elements',$num_elements)
            ->with('ruta_view', 'home');
    }



    public function store(BannerCreateRequest $request)
    {
        $this->element->posicion = $request->posicion;
        $this->element->tipo = 'Banner';

        $this->mover_posicion($request->posicion, 'Banner');
        
        if ( $request->hasFile('img_file') ){
            $this->save_element($request, 'img/banner');
        }

        return redirect()->route('banner.index');
    }


    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $elemento = Elemento::find($id);
            $this->devolver_posicion($elemento->posicion, 'Banner');
            $elemento->delete();
    
            DB::commit();

            session()->flash('success','Se eliminó la imagen exitosamente !!!');
            return redirect()->route('banner.index');
        
        } catch (\Throwable $th) {
            DB::rollback();
            session()->flash('danger','Ocurrio un error al borrar !!!');
            return redirect()->route('banner.index');
        }
    }

    public function create(){}
    public function edit($id){}
    public function show($id){}
    public function update(Request $request){}
    

}

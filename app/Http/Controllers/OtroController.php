<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ElementTrait;
use App\Traits\PosTrait;
use App\Traits\DocTrait;
use App\Traits\ImgTrait;

use App\Elemento;
use App\Pos;

use Storage;
use Auth;
use DB;

class OtroController extends Controller
{
    use ElementTrait;
    use PosTrait;
    use ImgTrait;
    use DocTrait;

    public function index()
    {
        $posts = Pos::where('tipo_pos_id',6)
            ->orWhere('tipo_pos_id',7)
            ->get();

        $elements = Elemento::where('tipo','Facebook')
            ->get();

        return view('admin.otros.index')
            ->with('elements',$elements)
            ->with('posts',$posts);
    }

    public function editar_pos($pos_id)
    {
        return view('admin.otros.edit_pos')
            ->with('pos',$this->edit_pos($pos_id));
    }

    public function editar_element($element_id)
    {
        $dat = $this->edit_element($element_id);

        return view('admin.otros.edit_element')
            ->with('element',$dat['element']);
    }

    public function update(Request $request, $obj_id)
    {
        if($request->type == 'pos'){

            return $this->pos_update($request, $obj_id, 'otros.edit_pos', 'otros.index');
        }
        elseif($request->type == 'element'){

            return $this->element_update($request, $obj_id, 'otros.edit_element','otros.index');
        }

    }

    public function delete_doc($obj_id)
    {
        $pos = Pos::find($obj_id);
        if($pos){
            $this->delete_doc($pos->doc_ruta);
            $pos->doc_name = '';
            $pos->doc_ruta = '';
            $pos->save();
        }

        return redirect()->route('otros.edit_pos',$obj_id);    
    }



}

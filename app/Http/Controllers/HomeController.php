<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\HomeTrait;
use Carbon\Carbon;
use App\Elemento;
use App\Empresa;
use App\Pos;


class HomeController extends Controller
{
	use HomeTrait;	

	/**
	 ** index() permite cargar la vista principal
	 **/
	public function index($noticia_id = null)
	{
		return view('index')
			->with('data',getDataIniHp())
			->with('noticia_id',$noticia_id);

	}

	public function show($noticia_id)
	{
		return view('index')
			->with('banner',hp_get_banner())
			->with('noticia',hp_get_noticia($noticia_id))
			->with('noticias',hp_get_noticias())
			->with('links',hp_get_noticias())
			->with('empresa',hp_get_empresa());
	}

	public function list()
	{
		$noticias = hp_get_noticias();

		$res = [
            'error' => false,
            'pagination' => [
                'total'         => $noticias->total(),
                'current_page'  => $noticias->currentPage(),
                'per_page'      => $noticias->perPage(),
                'last_page'     => $noticias->lastPage(), 
                'from'          => $noticias->firstItem(),
                'total'         => $noticias->total(),
                'to'            => $noticias->lastPage()
            ],
            'dat'   => $noticias
        ];


		return response()->json($res);
	}

}
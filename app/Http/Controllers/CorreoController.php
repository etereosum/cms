<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Correo;
use App\Imports\CorreosImport;
use Validator;
use Excel;
use Auth;
use File;
use DB;

class CorreoController extends Controller
{

    protected $data;

    public function __construct()
    {
        $this->middleware('auth');
    }
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.correos.index');
    }

    public function list()
    {
        $res = Correo::orderBy('updated_at','desc')->get();

        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'correo' => 'email|required|unique:correos'
        ]);

        if($validator->fails()){
            $res = [
                'error' => true,
                'message' => $validator->messages()
            ];
            return response()->json($res);
        }

        $correo = Correo::create([
            'correo' => $request->correo,
            'user_create_id' => Auth::user()->id
        ]);

        $res = [
            'error' => false,
            'dat'   => $correo
        ];

        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(),[
                'correo' => "email|required|unique:correos,correo,$id"
            ]);
    
            if($validator->fails()){

                $res = [ 'error' => true, 'message' => $validator->messages()];
                return response()->json($res);
            }
    
            $correo = Correo::where('id', $id)
                ->update([
                    'correo' => $request->correo,
                    'user_update_id' => Auth::user()->id
                    ]);
            
            $res = [
                'error' => false,
                'dat'   => $correo
            ];

        } catch (\Exception $e) {
            $res = [
                'error' => true,
                'message'   => $e->getMessage()
            ];
        } finally {
            return response()->json($res);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($correo_id)
    {
        DB::beginTransaction();

        try{
            $correo = Correo::find($correo_id);
            $correo->delete();
            DB::commit();

            $res = ['error' => false, 'message' => 'Registro borrado exitosamente'];

        } catch(\Exception $e){
            DB::rollback();
            $res = ['error' => true, 'message' => $e->getMessage()];
        } finally {
            return response()->json($res);
        }
    }

    public function search($elemento = null)
    {

        try {
            if(is_null($elemento)){
                $result = Correo::orderBy('updated_at','desc')->get();
            } else {
                $result = Correo::where('correo','like', '%'.$elemento.'%')->get();
            }
            $res = [
                'error' => false,
                'dat' => $result
            ];
        } catch (\Exception $e) {
            $res = [
                'error' => true,
                'message' => $e->getMessage()
            ];
        } finally {
            return response()->json($res);
        }
    }

    // public function upload(Request $request)
    // {
    //     $this->validate($request,['file' => 'required']);

    //     if ( $request->hasFile('file') ){
    //         $extension = File::extension($request->file->getClientOriginalName());
            
    //         if ($extension == "xlsx" || $extension == "xls" || $extension == "csv")
    //         {
    //             $path = $request->file->getRealPath();

    //             // $this->data = Excel::load( $path, function($reader){
    //             // })->get();

    //             $this->data = Excel::import(new CorreosImport,$path);
    //             if (!empty($this->data) && $this->data->count()){
    //                 // $this->save_correos();
    //                 dd('vamos bien');
    //             }
    //             else {
    //                 session()->flash('danger','No se puede procesar el archivo');
    //                 return redirect()->route('correos.index');
    //             }
    //         }
    //     }
    // }

    // public function save_correos()
    // {
    //     DB::beginTransaction();
    //     $exist_error_email = 0;
    //     $errors = [];
    //     try{
    //         foreach($this->data as $dat){
    //             if(is_valid_email($dat)){
    //                 $exist_error_email = 1;
    //                 array_push($errors, $dat);
    //             }
    //         }
    //         if($exist_error_email){
    //             session::flash('danger','Corrija los siguientes correos');
    //             return redirect()->route('correos.index');
    //         } else {
    //             dd('vamos bien');
    //         }
    //     } catch (\Exception $e) {

    //     }
    // }

    // function is_valid_email($str)
    // {
    //     $matches = null;
    //     return (1 === preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $str, $matches));
    // }
}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DB;

/** 
 *  Permite extraer elementos de un campo tipo enum... 
 *  Recibe el nombre de la tabla y el nombre
 *  de la columna ej: getEnumValues('user','rol')
 */


function getEnumValues($table, $column)
{
  $type = DB::select( 
    DB::raw("SHOW COLUMNS FROM $table WHERE Field = '$column'") )
    [0]->Type;

  preg_match('/^enum\((.*)\)$/', $type, $matches);

  $enum = array();

  foreach( explode(',', $matches[1]) as $value ){
    $v      = trim( $value, "'" );
    $enum   = array_add($enum, $v, $v);
  }
  return $enum;
}

function rutas_view($vista)
{
    switch ($vista) {
      case 'home':
        return 'home';
        break;
      case 'quienes somos':
        return 'quienes-somos.index2';
        break;
      default:
        # code...
        break;
    }
}

function contador_palabras($cadena, $tamanio)
{   
    $nueva_cadena = "";
    $numero_palabras = 0;
    $i = 0;

    for ($i=0; $i < strlen($cadena); $i++) {
        if($cadena[$i] == " "){
            $numero_palabras++;
        }
    }

    if($tamanio < $numero_palabras){

      $numero_palabras = 0;
      $i = 0;

      while( $numero_palabras <= $tamanio ){
          $nueva_cadena .= $cadena[$i];
          
          if($cadena[$i] == " "){
              $numero_palabras++;
          }
          $i++;
      }      
      return $nueva_cadena;
    }
    else {
      return $cadena;
    }
  }





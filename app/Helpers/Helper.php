<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Elemento;
use App\Empresa;
use App\Pos;
use DB;



function save_img($request)
{
    if ( $request->hasFile('img_file') )
    {
        $this->save_img($request);
    }
}

function hp_get_banner()
{
    return Elemento::where('tipo','Banner')
    ->orderBy('posicion')
    ->get();
}

function hp_get_empresa()
{
    return Empresa::find(1);
}

function hp_get_links()
{
    return Elemento::where('tipo','Link')
		  ->orderBy('posicion')
		  ->get();
}

function hp_get_noticia($noticia_id)
{
    return Pos::find($noticia_id);
}

function hp_get_noticias()
{
    return Pos::orderBy('created_at','desc')
				->where('tipo_pos_id',1)
				->paginate(5);
}

function diaHp($posicion)
{
    $dias = [
        'Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'
    ];

    return $dias[$posicion];
}

function mesHp($posicion)
{
    $meses = [
        'Enero', 'Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre',
        'Noviembre','Diciembre'
    ];

    return $meses[$posicion - 1];
}

function fechaEspHp($date)
{
    $dia = diaHp($date->dayOfWeek);
    $mes = mesHp($date->month);

    return $dia.", ".$date->day." ".$mes." ".$date->year;
}

function getDataIniHp()
{
    $empresa = Empresa::find(1);
    
    $banner  = Elemento::where('tipo','Banner')
                ->orderBy('posicion')
                ->get();
    $links   = Elemento::where('tipo','Link')
                ->orderBy('posicion')
                ->get();

    $fecha   = FechaEspHp(Carbon::now());

    return [
        'empresa' => $empresa,
        'banner'  => $banner,
        'links'   => $links,
        'fecha'   => $fecha
    ];
}

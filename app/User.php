<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'nombres','apellidos','estado','institucion','rol',
        'cargo_id','telefono','email','municipio_id','institucion',
        'user_create_id','user_update_id','password'
    ];

    public function cargo(){
        return $this->hasOne('App\Cargo','id','cargo_id');
    }

    public function municipio(){
        return $this->hasOne('App\Municipio','id','municipio_id');
    }

    public function user_create(){
        return $this->hasOne('App\User','id','user_create_id');
    }

    public function user_update(){
        return $this->hasOne('App\User','id','user_update_id');
    }
  
    protected $hidden = [
        'password','remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

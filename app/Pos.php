<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pos extends Model
{
    protected $table = 'pos';

    protected $fillable = [
        'estado','img_name','img_ruta','texto','titulo',
        'doc_name','doc_ruta','tipo_pos_id',
        'user_create_id','user_update_id'
    ];

    public function tipo(){
        return $this->hasOne('App\TipoPos','id','tipo_pos_id');
    }

    public function user_create(){
        return $this->hasOne('App\User','id','user_create_id');
    }

    public function user_update(){
        return $this->hasOne('App\User','id','user_update_id');
    }

}

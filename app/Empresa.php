<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';
    
    protected $fillable = [
        'nombre','nit','telefono_1','telefono_2','celular_1','celular_2',
        'direccion','email','horario','user_update_id'
    ];

    public function user_update(){
        return $this->hasOne('App\User','id','user_update_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institucion extends Model
{
    protected $table = 'instituciones';

    protected $fillable = [
        'nombre','direccion','municipio_id','telefono',
        'email','user_create_id','user_update_id'
    ];

    public function municipio(){
        return $this->hasOne('App\Municipio','id','municipio_id');
    }

    public function user_create(){
        return $this->hasOne('App\User','id','user_create_id');
    }

    public function user_update(){
        return $this->hasOne('App\User','id','user_update_id');
    }
}

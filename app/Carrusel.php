<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrusel extends Model
{
    protected $fillable=[
    	'nombre',
    	'descripcion',
    	'imagen'
    ];
}

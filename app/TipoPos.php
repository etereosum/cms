<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPos extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'nombre'
    ];

}

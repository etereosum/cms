<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $nombre;
    public $telefono;
    public $asunto;
    public $mensaje;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nombre, $telefono,$email,$asunto,$mensaje)
    {
        $this->nombre   = $nombre;
        $this->telefono = $telefono;
        $this->email    = $email;
        $this->asunto   = $asunto;
        $this->mensaje  = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        return $this->from($this->email)
            ->subject($this->asunto.' de '.$this->nombre)
            ->view('public.emails.send')
            ->with('mensaje',$this->mensaje)
            ->with('telefono',$this->telefono)
            ->with('nombre',$this->nombre);
    }
}

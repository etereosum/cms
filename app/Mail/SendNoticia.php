<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNoticia extends Mailable
{
    use Queueable, SerializesModels;
    public $noticia;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($noticia)
    {
        $this->noticia = $noticia;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contacto@asodir.org', 'ASODIR')
            ->subject('Noticia:'.' '. $this->noticia->titulo)
            ->view('public.emails.send_noticia')
            ->with('noticia',$this->noticia)
            ->with('link','http://127.0.0.1:8000/public/post_email/'.$this->noticia->id);
    }
}

<?php

namespace App\Traits;

use App\Elemento;
use Storage;
use Auth;
use DB;

trait ElementTrait
{
    public function edit_element($id)
    {
        $element  = Elemento::find($id);
        $num_elements   = Elemento::where('estado','Activo')->count();

        return [
                'element'       => $element,
                'num_elements'  => $num_elements
            ];
    }


    public function mover_posicion($posicion, $tipo)
    {
        $elements = DB::table('elementos')
            ->where('tipo',$tipo)
            ->where('posicion','>=',$posicion)
            ->get();

        foreach($elements as $element){
            DB::table('elementos')
                ->where('id',$element->id)
                ->update(['posicion' => $element->posicion + 1]);
        }
    }

    public function devolver_posicion($posicion, $tipo)
    {
        $elements = DB::table('elementos')
            ->where('tipo',$tipo)
            ->where('posicion','>',$posicion)
            ->get();

        foreach($elements as $element){
            DB::table('elementos')
                ->where('id',$element->id)
                ->update(['posicion' => $element->posicion - 1]);
        }
    }

    public function element_update($request, $element_id, $redirect, $return)
    {
        $element = Elemento::find($element_id);
        $element->fill($request->all());

        if ( $request->hasFile('img_file') ){
            $this->delete_img($element->ruta);
            $img = $this->save_img($request, 'img');
            $element->ruta = $img['img_ruta'];
        }

        if ( ! $element->isDirty() ) {
            session()->flash('info','Ningun cambio en registro');    
            return redirect()->route($redirect,$element_id);
        }

        $element->user_update_id  = Auth::user()->id;
        $element->save();

        session()->flash('success','Registro actualizado exitosamente !!!');    
        return redirect()->route($return);
    }

    public function save_element($request, $ubicacion)
    {
        $path = Storage::disk('public')->put($ubicacion,$request->file('img_file'));
            
        $this->element->nombre = $request->file('img_file')->getClientOriginalName();
        $this->element->ruta = $path;

        $this->element->save();
    }

    public function get_banner($banner_id)
    {
        $element = Elemento::find($banner_id);

        return Storage::disk('public')->download($element->ruta, $element->nombre);
    }

    public function delete_element($ruta)
    {
         Storage::disk('public')->delete( $ruta );
    }
}
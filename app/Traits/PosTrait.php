<?php

namespace App\Traits;
use App\Pos;
use Auth;

trait PosTrait
{
    public function edit_pos($id)
    {
        $pos  = Pos::find($id);
        return $pos;
    }

    public function pos_update($request, $pos_id, $redirect, $return)
    {
        $pos = Pos::find($pos_id);
        $pos->fill($request->all());

        if ( $request->hasFile('img_file') ){
            $this->delete_img($pos->img_ruta);
            $img = $this->save_img($request, 'img');
            $pos->img_name = $img['img_name'];
            $pos->img_ruta = $img['img_ruta'];
        }

        if ( $request->hasFile('doc_file') ){
            $this->delete_doc($pos->doc_ruta);
            $doc = $this->save_doc($request, 'adjuntos');
            $pos->doc_name = $doc['nombre'];
            $pos->doc_ruta = $doc['ruta'];
        }

        if ( ! $pos->isDirty() ) {
          
            session()->flash('info','Ningun cambio en registro');    
            return redirect()->route($redirect,$pos_id);
        }

        $pos->user_update_id  = Auth::user()->id;
        $pos->save();

        session()->flash('success','Registro actualizado exitosamente !!!');    
        return redirect()->route($return);
    }

}
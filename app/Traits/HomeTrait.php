<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Elemento;
use App\Empresa;
use App\Pos;
use Storage;
use DB;


trait HomeTrait
{

	public function get_noticias()
	{
		$this->noticias = Pos::orderBy('created_at','desc')
				->where('tipo_pos_id',1)
				->paginate(5);
	}

	public function get_banner()
	{
		$this->banner = Elemento::where('tipo','Banner')
			->orderBy('posicion')
			->get();

	}

	public function get_links()
	{
		$this->links = Elemento::where('tipo','Link')
		  ->orderBy('posicion')
		  ->get();
	}

	public function get_perfil()
	{
		return view('admin.dashBoard.perfil');
	}

	public function get_noticia($noticia_id)
	{
		$this->noticia = Pos::find($noticia_id);
	}

	public function get_empresa()
	{
		$this->empresa = Empresa::find(1);
	}

}

<?php

namespace App\Traits;

use App\Pos;    
use Storage;


trait DocTrait
{

    public function save_doc($request, $carpeta)
    {
        $path = Storage::disk('local')->put($carpeta,$request->file('doc_file'));
            
        $nombre = $request->file('doc_file')->getClientOriginalName();
        $ruta = $path;

        return ['nombre' => $nombre, 'ruta' => $ruta ];
    }


    public function get_document($obj)
    {
        return Storage::download($obj->doc_ruta, $obj->doc_name);
    }

    public function delete_doc($ruta)
    {
        Storage::disk('local')->delete( $ruta );
    }

    public function show_document($id,$nombre)
    {
        $pos = Pos::find($id);
        $file= storage_path(). '/app/'.$pos->doc_ruta; 
        $headers = [ 
            'Content-Type' => 'application/pdf'
        ]; 

        return response()->file($file); 
    }
}

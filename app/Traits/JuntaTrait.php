<?php

namespace App\Traits;
use DB;

trait JuntaTrait
{
    /** 
     * recibe un entero posicion y agrega el elemento 
     * en la posicion especificada corriendo el resto de
     * elementos
     */
    public function agregarPosicionJt($posicion)
    {
        $integrantes = DB::table('users')
            ->where('rol','Junta directiva')
            ->where('posicion','>=',$posicion)
            ->get();

        foreach($integrantes as $integrante){
            DB::table('users')
                ->where('id',$integrante->id)
                ->update(['posicion' => $integrante->posicion + 1]);
        }
    }


    /**
     * recibe un entero posicion y el id de un user rol "Junta directiva"
     * cambiando la posicion de los elementos
     */
    public function actualizarPosicionJt($posicion, $integrante_id = null)
    {
        $integrantes = DB::table('users')
                ->where('rol','Junta directiva')
                ->where('id','!=',$integrante_id)
                ->orderBy('posicion')
                ->get();
        $array =  [];
        $j = 0;

        for ($i=0; $i < count($integrantes) + 1; $i++) { 
            if($posicion == $i + 1){
                array_push($array, $integrante_id);
            } else {
                $integrante = $integrantes[$j];
                array_push($array, $integrante->id);
                $j++;
            }
        }

        $n = 1;
        foreach($array as $e){
            DB::table('users')->where('id',$e)->update(['posicion' => $n]);
            $n++;
        }
    }

    public function eliminarPosicionJt($posicion)
    {
        $integrantes = DB::table('users')
                ->where('rol','Junta directiva')
                ->where('posicion','>',$posicion)
                ->orderBy('posicion')
                ->get();

        foreach($integrantes as $integrante){
            DB::table('users')
                ->where('id',$integrante->id)
                ->update(['posicion' => $integrante->posicion - 1]);
        }
    }
}

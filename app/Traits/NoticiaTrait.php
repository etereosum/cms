<?php

namespace App\Traits;

use App\Pos;
use App\Noticia;    
use Storage;


trait NoticiaTrait
{
    /**
     * Permite guardar una imagen en la carpeta 
     * public/img
     * Recibe un request 
     * PG 22/03/2019
     */

    public function save_img($request)
    {
        $path = Storage::disk('public')->put('img/noticias',$request->file('img_file'));

        $this->noticia->img_name = $request->file('img_file')->getClientOriginalName();
        $this->noticia->img_ruta = $path;
    }

    /**
     * Permite borrar una imagen
     * accediendo por el pos a esta
     * PG 22/03/2019
     */

    public function delete_img()
    {
        Storage::disk('public')->delete( $this->noticia->img_ruta );
    }

}
<?php

namespace App\Traits;

use App\Elemento;
use App\Pos;
use Storage;
use DB;



trait QuienesSomosTrait
{
    public function getHistoriaQst()
    {
        $this->historia = Pos::find(1);
    }

    public function getHorizonteQst()
    {
        $this->horizonte = Pos::find(3);
    }

    public function getEstatutosQst()
    {
        $this->estatutos = Pos::find(4);
    }

    public function save_doc_estatutos($request)
    {
        $path = Storage::disk('local')->put('estatutos',$request->file('doc_file'));
            
        $this->estatutos->doc_name = $request->file('doc_file')->getClientOriginalName();
        $this->estatutos->doc_ruta = $path;
    }

    public function delete_doc_estatutos()
    {
        Storage::disk('local')->delete( $this->estatutos->doc_ruta );
    }
}
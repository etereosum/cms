<?php

namespace App\Traits;

use App\Pos;
use Storage;
use DB;

trait ImgTrait
{
    public function save_img($request, $ruta)
    {
        $path = Storage::disk('public')->put($ruta,$request->file('img_file'));

        $img_name = $request->file('img_file')->getClientOriginalName();
        $img_ruta = $path;

        return [
            'img_name' => $img_name,
            'img_ruta' => $img_ruta
        ];
    }

    public function delete_img($img_ruta)
    {
        Storage::disk('public')->delete( $img_ruta );
    }

}
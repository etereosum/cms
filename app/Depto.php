<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Depto extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'nombre','codigo'
    ];

    public function municipios(){
        return $this->hasMany('App\Municipio','depto_id','id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'nombre','codigo','depto_id'
    ];

    public function depto(){
        return $this->hasOne('App\Depto','id','depto_id');
    }

}

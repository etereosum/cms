# **CMS**

Administrador de contenido desarrollado para una asociación educativa. El aplicativo tiene un panel administrativo que permite gestionar todo el contenido, permite crear noticias, subir documentación y enviar notificaciones a través de correo electrónico a los correos registrados.

Para ingresar al panel administrativo utilice cualquiera de los usuarios existentes en database/sedes/UsersTableSeeder.php, siéntase libre de explorar el software.

## Requerimientos

Composer

PHP 7.2

Node JS 12.\*

Laravel 5.8

Mysql 8.\*

Git 2.\*

## Instalación

Clone el repositorio

\$ git clone [https://gitlab.com/etereosum/cms.git](https://gitlab.com/etereosum/cms.git)

Cree un archive .env para configurar las variables

\$ cd cms

\$ cp .env-example .env

\$ vim .env

Configure el environment agregando una base de datos y su usuario de mailtrap [https://mailtrap.io/](https://mailtrap.io/)

DB_CONNECTION=mysql

DB_HOST=127.0.0.1

DB_PORT=3306

DB_DATABASE=cms

DB_USERNAME=root

DB_PASSWORD=my_password

&nbsp;

MAIL_DRIVER=smtp

MAIL_HOST=smtp.mailtrap.io

MAIL_PORT=2525

MAIL_USERNAME=username_generado_mailtrap

MAIL_PASSWORD=password_generado_mailtrap

MAIL_ENCRYPTION=tls

&nbsp;

Genere una llave

\$ php artisan key:generate

Actualice dependencias y cargue nuevamente las variables de configuración

\$ composer update

\$ php artisan config:cache

Cree la base de datos, ejecute la migración y genere los datos de prueba.

\$ php artisan migrate

\$ php artisan db:seed

Inicie el servidor

\$ php artisan serve

Le recordamos que los usuarios para ingresar al panel administrativo están en el seeder UsersTableSeeder, para ingresar vaya al home (localhost:8000), de click en ingresar y agregue las credenciales.

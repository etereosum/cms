@extends('index_quienes_somos')
@section('contenido')

<div>
	<section class="col-md-8 offset-md-2">
		<header class=" font-weight-bold text-uppercase">
			<center>
				<h4>{{$pos->titulo}}</h4>
			</center>
		</header>		
		<hr>	
		@if($pos->img_ruta)		
			<img src="{{asset($pos->img_ruta)}}" width="100%" height="300" alt="" class="text-justify">
			<hr>
		@endif
		<p style="font-size: 10px;margin: -13px 0px;">
			{{$pos->created_at->format('d-m-Y')}} - {{$pos->tipo->nombre}}</p>
		<br>
		<p>{!! $pos->texto !!}</p>

		@include('public.template.iconos')
	</section>

			
</div>

@endsection
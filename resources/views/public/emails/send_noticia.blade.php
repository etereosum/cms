<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <p style="color:red">Se ha a gregado una nueva noticia en nuestro sitio web www.asodir.org</p>

    <h1>{{ $noticia->titulo }}</h1>
    <p>{!! $noticia->texto !!} . . . <a href="{{$link}}">leer mas</a></p>

    <label class="form-text text-muted">El contenido de este mensaje y sus anexos son únicamente para el uso del destinatario y pueden contener información  clasificada o reservada. Si usted no es el destinatario intencional, absténgase de cualquier uso, difusión, distribución o copia de esta comunicación.</label>

</body>
</html>
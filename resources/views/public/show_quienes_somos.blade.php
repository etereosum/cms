<!--menú banner-->

<div class="container-fluid ">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <div class="row">
              <section class="m-3 text-justify">
                 @yield('contenido')
              </section>         
            </div>
        </div>
      </div>
</div>
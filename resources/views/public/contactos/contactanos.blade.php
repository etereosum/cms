@extends('index2')
@section('contenido')

@section('mapa')
     <br>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3975.7263133011597!2d-75.69864568551931!3d4.816986241108725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e38874eb4d58cf1%3A0x96a5236f4d0edb1a!2sCOOEDUCAR!5e0!3m2!1ses-419!2sco!4v1556773198130!5m2!1ses-419!2sco" 
      width="100%" height="55%" frameborder="0" style="border:0" allowfullscreen></iframe>   
@endsection
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"  style="background:#e3342f; height: 45px" >
        <h5 class="modal-title font-fuent" >Escrí​benos</h5>
      </div>
      <div class="modal-body">

        @include('librerias.error')

        <form method="POST" action="{{route('contactos.send_message')}}">

          @csrf
          <div class="form-group">
            <label for="contactanos" >¿Tu comentario está asociado a? *</label>
            <select name="asunto" class="form-control form-control-sm">
              <option selected disabled>-------------------------------Seleccionar----------------------------------</option>
              <option value="Felicitaciones">Felicitaciones</option>
              <option value="Inquietud">Inquietud</option>
              <option value="Inquietud">Solicitud de información</option>
              <option value="Quejas">Quejas</option>
              <option value="Sugerencia">Sugerencia</option>
              <option value="Otro">Otro</option>
            </select>
          </div>

          <div class="form-group">
            <label for="nombre">Nombre:</label>
            <input type="text" id="nombre" class="form-control form-control-sm" name="nombre">
          </div>
          <div class="form-group">
            <label for="email" >Correo electronico:</label>
            <input type="email" id="email" class="form-control form-control-sm" name="email">
          </div>
          <div class="form-group">
            <label>Institución educativa:</label>
            <input type="text" id="institucion" class="form-control form-control-sm" name="institucion">
          </div>

          <div class="form-group">
            <label>Teléfono:</label>
            <input type="text" id="telefono" class="form-control form-control-sm" name="telefono">
          </div>

          <div class="form-group">
            <label>Mesensaje:</label>
            <textarea rows="4" class="form-control form-control-sm" name="mensaje"></textarea>
          </div>
       
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary">Cerrar</button>
            <input type="submit" class="btn btn-primary" value="Enviar">
          </div>

       </form>
    </div>
  </div>
  @endsection
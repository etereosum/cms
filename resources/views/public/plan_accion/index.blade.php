@extends('index2')
@section('contenido')
@include('librerias.libreria')


<!------------------------------------------------------------>
@foreach($plan as $pos)
	<section class="pt-4 ml-3">
			<header class=" font-weight-bold text-uppercase"><h4>{{$pos->titulo}}</h4></header>
			<P></P>			
			<img src="{{asset($pos->img_ruta)}}" width="100%" height="260" alt="" class="text-justify">
	</section>


	<div  class="m-3 font-fuent-black text-justify pb-5">
		<section class="mostrar">
		
		 {!!$pos->texto!!}
		</section>
	
		
		@include('public.template.iconos')
			
				

	</div>

@endforeach

  <script>
	$(function() {
		$('section.mostrar').expander({
		slicePoint: 200, // si eliminamos por defecto es 100 caracteres
		expandText: 'Leer mas...', // por defecto es 'read more...'
		expandPrefix: '&nbsp;', //valor de espacio por defecto es '&hellip; ',
		collapseTimer: 0, // tiempo de para cerrar la expanción si desea poner 0 pra no cerrar
		userCollapseText: 'ocultar' // por defecto es 'read less...'
	  });
	});
  </script>

<!--Libreria JS -->

@endsection
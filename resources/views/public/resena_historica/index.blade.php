@extends('index_quienes_somos')
@section('contenido')
@include('librerias.libreria')


<div class="container font-fuent-black text-justify">
	
	<!-- HISTORIA -->

	<header class=" text-center pb-3" style="margin-top: 1.8rem">
		<h4>{{$historia->titulo}}</h4>
		<hr style="border-top: 1px solid #08743b; width:10%;">
	</header>
	@if($historia->img_name)
		<center>
			<img src="{{asset($historia->img_ruta)}}" width="600">
		</center>
		<br>
	@endif

	<section class="mostrar">
		{!! $historia->texto !!}
	</section>

	<br>

	<!-- HORIZONTE INSTITUCIONAL -->

	<header class=" text-center pb-3" style="margin-top: 1.8rem">
		<h4>{{ $horizonte->titulo }}</h4>
		<hr style="border-top: 1px solid #08743b;width:10%;">
	</header>

	@if($horizonte->img_name)
		<center>
			<img src="{{asset($horizonte->img_ruta)}}" width="600">
		</center>
		<br>
	@endif
	<section class="mostrar">
		{!! $horizonte->texto !!}
	</section>

	<br>
	<!-- ESTATUTOS -->

	<header class=" text-center pb-3" style="margin-top: 1.8rem">
		<h4>{{ $pos->titulo }}</h4>
		<hr style="border-top: 1px solid #08743b;width:10%;">
	</header>

	@if($pos->img_name)
		<center>
			<img src="{{asset($pos->img_ruta)}}" width="600">
		</center>
		<br>
	@endif
	<section class="mostrar">
		{!! $pos->texto !!}
	</section>
	<div style="font-size:10px;">
		@include('public.template.iconos')
	</div>
</div>

  <script>
	$(function() {
		$('section.mostrar').expander({
		slicePoint: 500, // si eliminamos por defecto es 100 caracteres
		expandText: 'Leer mas...', // por defecto es 'read more...'
		expandPrefix: '&nbsp;', //valor de espacio por defecto es '&hellip; ',
		collapseTimer: 0, // tiempo de para cerrar la expanción si desea poner 0 pra no cerrar
		userCollapseText: 'ocultar' // por defecto es 'read less...'
	  });
	});
  </script>

@endsection
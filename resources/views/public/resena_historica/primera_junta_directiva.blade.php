<table class="table table-striped font-table">
		  <tbody>
		  	<tr>
		  		<th> Presidente</th>
		  		<td>ARLEY PELÁEZ ARIAS (ASORIS)</td>
		  	</tr>
		  	<tr>
		  		<th> Vice-presidente</th>
		  		<td>JOSE HUMBERTO BUSTAMANTE L. (Q.E.P.D.)</td>
		  	</tr>
		  	<tr>
		  		<th>Secretaria</th>
		  		<td> MARIA SADITH  VILLA DE GÓMEZ</td>
		  	</tr>
		  	<tr>
		  		<th>Tesorera</th>
		  		<td>BERTHA ELISA RAMIREZ GUEVARA </td>
		  	</tr>
		  	<tr>
		  		<th>Fiscal	</th>
		  		<td>LUIS ROSENDO CASTAÑO MORENO</td>
		  	</tr>
		  
		  	<tr>
		  		<th rowspan="5" style="align-content: center;">Suplentes</th>
		  		<td>CARLOS HUMBERTO ARANGO A. </td>
		  	</tr>
		  	<tr>
		  		<td>TERESA LOAIZA  DE M.</td>
		  	</tr>
		  	<tr>
		  		<td>VICTOR JULIO PINTO (Q.E.P.D.)</td>
		  	</tr>
		  	<tr>
		  		<td>JESÚS ANTONIO JARAMILLO (Q.E.P.D.)</td>
		  	</tr>
		  	<tr>
		  		<td>HENRY BETANCUR MONCADA</td>
		  	</tr>

		  </tbody>
		</table>
	
	
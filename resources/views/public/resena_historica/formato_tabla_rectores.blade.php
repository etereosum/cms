<table class="table table-striped font-table align-middle">
  <thead>
    <tr>
      <th scope="col">RECTOR(A)</th>
      <th scope="col">INSTITUCION</th>
      <th scope="col">MUNICIPIO</th>    
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">CARLOS HUMBERTO ARANGO</th>
      <td>JARDIN PEREIRA</td>
      <td>PEREIRA</td>
    </tr>
    <tr>
      <th scope="row">ARLEY PELAEZ A </th>
      <td>JUVENAL CANO MORENO</td>
      <td>PEREIRA</td>
    </tr>
     <tr>
      <th scope="row">HUMBERTO BUSTAMANTE B</th>
      <td>RAFAEL URIBE URIBE</td>
      <td>PEREIRA</td>
    </tr>
    <tr>
      <th scope="row">VICTOR JULIO PINTO</th>
      <td>DEOGRACIAS CARDONA NOCTURNO (Q.E.P.D) </td>
      <td>PEREIRA</td>
    </tr>
     <tr>
      <th scope="row">HERMANA HELENA DIAZ P </th>
      <td>I. LESTONAC </td>
      <td>PEREIRA</td>
    </tr>
      <tr>
      <th scope="row">MARIA SADITH VILLA DE G</th>
      <td>INSTITUTO FEMENINO LA VILLA </td>
      <td>PEREIRA</td>
    </tr>
    <tr>
      <th scope="row">TERESA  LOAIZA DE M</th>
      <td>I.FEMENINO KENNEDY </td>
      <td>PEREIRA</td>
    </tr>
    <tr>
      <th scope="row">BERTHA ELEISA RAMIREZ </th>
      <td>PEDRO J.MARÍN (N)</td>
      <td>PEREIRA</td>
    </tr>
     <tr>
      <th scope="row">JOSÉ HENRY BETANCUR M</th>
      <td>JESUS MARÍA ORMAZA</td>
      <td>PEREIRA</td>
    </tr>
    <tr>
      <th scope="row">JULIO RIVAS C</th>
      <td>I.TECNICO SUPERIOR  (D)</td>
      <td>PEREIRA</td>
    </tr>
     <tr>
      <th scope="row">FABIO ERNESTO BETANCUR</th>
      <td>INEM </td>
      <td>PEREIRA</td>
    </tr>
     <tr>
      <th scope="row">LUIS ROSENDO CASTAÑO </th>
      <td>FABIO VASQUEZ  </td>
      <td>DOSQUEBRADAS </td>
    </tr>
      <tr>
      <th scope="row">HUMBERTO VELASQUEZ Q</th>
      <td>JUAN HURTADO</td>
      <td>BELEN DE UMBRIA </td>
    </tr>
    <tr>
    <th scope="row">HERNANDO MURILLO  A</th>
      <td>COLEGIO RURAL COMBIA </td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">FRANCISCO VEGA (Q.E.P.D)</th>
      <td>I.TECNICO  </td>
      <td>SANTA ROSA </td>
	</tr>
	 <tr>
    <th scope="row">JESÚS AJARAMILLO</th>
      <td>DEOGRACIAS CARDONA (D)</td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">RICARDO MARISCAL M</th>
      <td>SANTO TOMÁS</td>
      <td>APIA</td>
	</tr>
	<tr>
    <th scope="row">URIEL VALENCIA M</th>
      <td>COLEGIO RURAL TAPARCAL</td>
      <td>BELEN DE UMBRIA </td>
	</tr>
	<tr>
    <th scope="row">ARMANDO HOYOS V	</th>
      <td>GONZALO MEJÍA ECHEVERRY ALTAGRACIA</td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">OCTAVIO MESA NOREÑA	</th>
      <td>HECTOR ANGEL ARCILA LA FLORIDA</td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">ALEXANDER AGUDELO V	</th>
      <td>ALFONSO JARAMILLO </td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">ARGELIA PINZÓN DE MARTINEZ</th>
      <td>GIMNASIO RISARALDA</td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">HERNANDO GUERRERO</th>
      <td>NORMAL SUPERIOR</td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">HERNANDO GARZÓN A (QEPD)</th>
      <td>PEDRO URIBE MEJÍA</td>
      <td>SANTA ROSA</td>
	</tr>
	<tr>
    <th scope="row">CARMEN NULBIA OSPINA </th>
      <td>AGUSTIN NIETO CABALLERO FRAILES </td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">FANY ESCOBAR SALAZAR</th>
      <td>LORENCITA VILLEGAS</td>
      <td>SANTA ROSA </td>
	</tr>
	<tr>
    <th scope="row">LIBIA VALENCIA</th>
      <td>LABOURÉ</td>
      <td>SANTA ROSA </td>
	</tr>
	<tr>
    <th scope="row">MARTIN ALONSO ALVAREZ </th>
      <td>SAN FRANCISCO DE ASIS ARABIA </td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">ANA MARIA PULGARIN </th>
      <td>MARIA AUXILIADORA </td>
      <td>SANTUARIO</td>
	</tr>
	<tr>
    <th scope="row">RAMÓN ELIAS HENAO  </th>
      <td>INSTITUTO AGRICOLA SANTA ANA</td>
      <td>GUATICA</td>
	</tr>
	<tr>
    <th scope="row">LUIS HUMBERTO VELÁSQUEZ Q</th>
      <td>JUAN HURTADO </td>
      <td>BELEN DE UMBRIA</td>
	</tr>
	<tr>
    <th scope="row">ROGELIO ESPINAL </th>
      <td>PABLO VI (D) </td>
      <td>DOSQUEBRADAS</td>
	</tr>
	<tr>
    <th scope="row">CARMEN LUCÍA DOMINGUEZ </th>
      <td>JESÚS MARIA ORMAZA III JORNADA </td>
      <td>PEREIRA</td>
	</tr>
	<tr>
    <th scope="row">MARÍA NORBY ARANGO CHICA</th>
      <td>AQUILINO BEDOYA (N).</td>
      <td>PEREIRA</td>
	</tr>
   
  </tbody>
</table>
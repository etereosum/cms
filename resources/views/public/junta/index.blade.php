@extends('index2')
@section('contenido')

<div class="pt-4">
	<h5 style="font-family: verdana;">JUNTA DIRECTIVA</h5><P></P>
	@foreach($junta as $integrante)		
		<P>
			<div class="card mb-3" style="max-width: 540px;">
			  <div class="row no-gutters">
			    <div class="col-md-4 d-flex align-items-center justify-content-center">
			      <h4 class="card-title " ><center>{{ $integrante->cargo->nombre }}</center></h4>
			    </div>
			    <div class="col-md-8">
			      <div class="card-body" style="font-family: verdana;">
			       <h5>{{ $integrante->nombres}} {{ $integrante->apellidos}}</h5>
		     		 <h5>{{$integrante->institucion}}</h5>	
			      </div>
			    </div>
			  </div>
			</div>

		</P>
	@endforeach
</div>
@endsection
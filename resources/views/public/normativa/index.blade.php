@extends('index2')
@section('contenido')
@include('librerias.libreria')
 <div class="pt-1">
              
        @if($encabezado->img_ruta)  
		<section style="margin-top:11px;">
          <img src="{{asset($encabezado->img_ruta)}}" width="100%" height="317" alt="" class="text-justify">
		</section>
        @endif        	
		   <br>
		   
			<section>
				<h5>{{ $encabezado->titulo }}</h5>
				<p>
					{!! $encabezado->texto !!}
				</p>
			</section>

		<section>

		<hr style="border-top: 1px solid #08743b; width:20%;">
		<br>
				@foreach($normativas as $pos)
				
					<div class="row no-gutters">

						<div class="col-md-12">
							<div class="card-body" style="padding:0px;">
								<h5 style="color: red">
									{{ $pos->titulo }}
								</h5>

								<p class="card-text"><small class="text-muted">Publicado: {{ $pos->created_at->format('d/m/Y') }} </small></p>
								<p class="mostrar" style="line-height:17px;font-size:0.8rem;margin-bottom:9px;margin-top:8px;">{!! $pos->texto !!}</p>
								
								@include('public.template.iconos')
							</div>
						</div>
					</div>
					<hr style="border-top: 1px solid #08743b; width:100%;opacity:0.1;">
				@endforeach
			
			</section>
  		<div class="row">
      		<div class="col-md-3"></div>
    		<div class="col-md-9" style="padding-left:27px;">
    			{{$normativas->links()}}
    		</div>
    	</div>
	</div>	

      	<script>
			$(function() {
				$('section.mostrar').expander({
				slicePoint: 250, // si eliminamos por defecto es 100 caracteres
				expandText: 'Leer mas...', // por defecto es 'read more...'
				expandPrefix: '&nbsp;', //valor de espacio por defecto es '&hellip; ',
				collapseTimer: 0, // tiempo de para cerrar la expanci贸n si desea poner 0 pra no cerrar
				userCollapseText: 'ocultar' // por defecto es 'read less...'
			  });
			});
  		</script>


  @endsection
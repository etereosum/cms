@extends('index2')
@section('contenido')
@include('librerias.libreria')

<!------------------------------------------------------------>
@foreach($sgsst as $sg)
	<section class="pt-4 ml-3">
			<header class=" font-weight-bold text-uppercase"><h4>{{$sg->titulo}}</h4></header>		
	</section>

	<div  class="mr-3 ml-3 font-fuent-black text-justify pb-5">
		<section class="ver">
		<p v-html=""> {!! $sg->texto !!}</p>
		</section>
		{{-- @include('public.template.iconos') --}}
	</div>

@endforeach

<script>
	$(function() {
		$('section.ver').expander({
		slicePoint: 200, // si eliminamos por defecto es 100 caracteres
		expandText: 'Leer mas...', // por defecto es 'read more...'
		expandPrefix: '&nbsp;', //valor de espacio por defecto es '&hellip; ',
		collapseTimer: 0, // tiempo de para cerrar la expanción si desea poner 0 pra no cerrar
		userCollapseText: 'ocultar' // por defecto es 'read less...'
	  });
	});
  </script>

@endsection
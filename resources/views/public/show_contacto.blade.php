<!--menú banner-->
<div class="container-fluid ">
  
        <div class="row">
          <div class="col-md-10 offset-md-1">
            <div class="row">
              <div class="col-md-5 " style="margin-top: 1.9rem">
              @include('public.template.menu_banner_principal')       
            </div> 
          
            <div class="col-md-7 ">
              <section class="m-3 text-justify">
                 @yield('contenido')
              </section>         
            </div>
        </div>
      </div>
    </div>
</div>
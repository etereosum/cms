<!--Link-->

  
<div class="row" >
	<div class="col-md-8 offset-md-2" style="margin-top:40px;">
		
		<div class="autoplay">

			@foreach( $data['links'] as $link )

				<a href="{!! $link->link !!}" target="_blank" style="width:400px;height:120px;;">
					<img src="{{ asset($link->ruta) }}" style="width:100%;height:100%;padding: 0px 20px 0px 20px;">
				</a>

			@endforeach

		</div>
	</div>
</div>


<script type="text/javascript" src="{{asset('js/jquery-migrate.js')}}"></script>
<script type="text/javascript" src="{{asset('slick/slick.min.js')}}"></script>

<script type="text/javascript">
	$('.autoplay').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000
	});
</script>

		    

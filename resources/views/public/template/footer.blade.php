<!--footer-->
<div class="container-fluid footer">
  
        <div class="row">
          <div class="col-md-8 offset-md-2 ">
            <div class=" text-center ">

              @php
                $empresa = $data['empresa']
              @endphp

              {{ $empresa->nombre }}<br>
              {{ $empresa->direccion }} Pereira Risaralda Teléfono: {{ $empresa->telefono_1 }} Movil: {{ $empresa->celular_1 }}<br>
        				{{ $empresa->email }}  Horario de atención {{ $empresa->horario }}<br><br>
        			© Derechos reservados
        			
            </div>  
          </div>
      
        </div>
</div>

	<div  class="container-fluid " style="background:#08743b;">
			@include('public.formulariosModales.login')
		<div class="row">
		    <div class="col-md-8 offset-md-2" >
				<nav class="main-menu navbar-light">

					<a href="{{ url('/') }}" class="active">Inicio</a>
					<a href="{{ route('quienes-somos.index2') }}">Quiénes somos</a>
					<a href="{{ route('normativa_public.index') }}">Normativa</a>
					<a href="{{ route('contactos.index') }}">Contactos</a>

						@if(Auth::user())

							<a href="{{ route('dash.index') }}" target="_blank">Admin.. {{ Auth::user()->nombres}}</a>

						@else

							<a href="#" data-toggle="modal" data-target="#login">Ingresar</a>

						@endif
				</nav>
			</div>
		</div>
	</div> 

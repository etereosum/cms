
@include('public.template.hora')
<!--LOGO/REDES-->
<div class="container-fluid ">
  
        <div class="row ">
          <div class="col-md-8 offset-md-2">
            <div class="row">
              <div class="col-md-6 mb-2 " style="padding:10px 0px;">
               <img src="{{ asset('img/logo_asodir/logo.svg')}} " alt="" style="width: 600px; height: 160px;" class="align-self-center img-fluid">
              </div>  
          
              <div class="col-md-6 align-self-center text-right mb-2  ">

                    <div style="margin: 31px 0px 0px 0px;">  
                      <a href=""><img class="img-link mr-2" src="{{ asset('img/img_redes_sociales/FACEBOOK.svg') }}" alt=""></a>
                      <a href=""><img class="img-link mr-2" src="{{ asset('img/img_redes_sociales/INSTAGRAM.svg') }}" alt=""></a>
                      <a href=""><img class="img-link mr-2" src="{{ asset('img/img_redes_sociales/TWITER.svg') }}" alt=""></a>
                      <a href=""><img class="img-link " src="{{ asset('img/img_redes_sociales/WHATSAPP.svg') }}" alt=""></a>
                    </div>
                       <div class="mt-4 d-flex justify-content-end" id="search">    
                        <input type="search" v-model="string" placeholder="Buscar"  v-on:keyup.13="search()"
                          class="form-control form-control-sm" style= width:250px>   
                      </div>
              </div>

            </div>
          </div>
      
        </div>
</div>

<script>

  var search = new Vue({
    el: '#search',
    data: {
      string: ''
    },
    methods:{
      search(){
        var route = "{{url('public/search/get')}}/" + this.string
        document.location.href= route;
      }
    }
  })

</script>




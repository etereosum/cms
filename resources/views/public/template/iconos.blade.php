    @if($pos->doc_name != '' || $pos->doc_name != null)  
        @php
			$info = new SplFileInfo($pos->doc_name);
			$resultado= $info->getExtension();
		@endphp

		@switch($resultado)
    		@case('docx')
        		<dt><a href="{{ route('public.document-pos.show',[$pos->id,$pos->doc_name]) }}" target="_blanck">
                    <img src="{{ asset('img/descargar/word.svg') }}" width="40px" height="40" class="mr-2  ">
                    {{ $pos->doc_name }}</a></dt>
        	@break
        	@case('doc')
        		<dt><a href="{{ route('public.document-pos.show',[$pos->id,$pos->doc_name]) }}" target="_blanck">
                    <img src="{{ asset('img/descargar/word.svg') }}" width="40px" height="40" class="mr-2 ">{{ $pos->doc_name }}</a></dt>
        	@break
    		@case('pdf')
        		<dt><a href="{{ route('public.document-pos.show',[$pos->id,$pos->doc_name]) }}" target="_blanck">
                    <img src="{{ asset('img/descargar/pdf.png') }}" width="40px" height="40" class="mr-2 ">{{ $pos->doc_name }}</a></dt>
        	@break
        	@case('xlsx')
        		<dt><a href="{{ route('public.document-pos.show',[$pos->id,$pos->doc_name]) }}" target="_blanck">
                    <img src="{{ asset('img/descargar/excel.svg') }}" width="40px" height="40" class="mr-2 ">{{ $pos->doc_name }}</a></dt>
        	@break
            @case('xls')
                <dt><a href="{{ route('public.document-pos.show',[$pos->id,$pos->doc_name]) }}" target="_blanck">
                    <img src="{{ asset('img/descargar/excel.svg') }}" width="40px" height="40" class="mr-2 ">{{ $pos->doc_name }}</a></dt>
            @break
        	@case('rar')
        		<dt><a href="{{ route('public.document-pos.show',[$pos->id,$pos->doc_name]) }}" target="_blanck">
                    <img src="{{ asset('img/descargar/rar.svg') }}" width="40px" height="40" class="mr-2 ">{{ $pos->doc_name }}</a></dt>
        	@break
        	@case('zip')
        		<dt><a href="{{ route('public.document-pos.show',[$pos->id,$pos->doc_name]) }}" target="_blanck">
                    <img src="{{ asset('img/descargar/rar.svg') }}" width="40px" height="40" class="mr-2 ">{{ $pos->doc_name }}</a></dt>
        	@break
        	@default
        		<dt><a href="{{ route('public.document-pos.show',[$pos->id,$pos->doc_name]) }}" target="_blanck">
                    <img src="{{ asset('img/descargar/generico.svg') }}" width="40px" height="40" class="mr-2 ">{{ $pos->doc_name }}</a></dt>
		@endswitch	
    @endif
<!--publicaciones-->
<div class="container-fluid ">
  
        <div class="row">
          <div class="col-md-10 offset-md-1 ">
            <div class="row">
                <div class="col-md-5 pt-4">
                  <div class="card my-2" ><!--Inicio Card  Menú Principal -->
                    <span class="card-header font-panel-titulo"style="background:#e3342f">Publicaciones</span>        
                    <div class="card-body ml-3 mr-3 font-panel-texto">
                      @include('public.publicaciones.list')  
                    </div>
                  </div>
                </div>  
          
                <div class="col-md-7 ">
                  @include('public.publicaciones.show')
                </div>  
            </div>
        </div>
      </div>
  </div>
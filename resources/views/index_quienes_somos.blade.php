<body style="margin:0px 0px 0px 0px;background:#fffff0;">
	@include('librerias.libreria')
	<!--header-->
	@include('public.template.header')

	<!--navbar-->
	@include('public.template.navbar_propia')
	
	@include('public.show_quienes_somos')

	@include('public.template.link')

	<!--footer-->
	@include('public.template.footer')
	
</body>

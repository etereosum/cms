  <script>
	$(function() {
		$('div.mostrar').expander({
		slicePoint: 500, // si eliminamos por defecto es 100 caracteres
		expandText: 'Leer mas...', // por defecto es 'read more...'
		expandPrefix: '&nbsp;', //valor de espacio por defecto es '&hellip; ',
		collapseTimer: 0, // tiempo de para cerrar la expanción si desea poner 0 pra no cerrar
		userCollapseText: 'ocultar' // por defecto es 'read less...'
	  });
	});
  </script>
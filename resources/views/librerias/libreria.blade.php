<link rel="stylesheet" href="{{ asset('css/misestilos.css') }}">
<link rel="stylesheet" href="{{ asset('css/nav-bar.css') }}">

<link rel="stylesheet" href="{{ asset('css/redes/font.css') }}">
<link rel="stylesheet" href="{{ asset('css/redes/main.css') }}">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('css/dashBoard.css') }}">
<link rel="stylesheet" href="{{ asset('css/login.css') }}">

<link rel="stylesheet" type="text/css" href="{{asset('slick/slick.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick-theme.css')}}" />

<script src="{{ asset('js/app.js') }}"></script>
<!--Libreria para data table-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script> 
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-expander/1.7.0/jquery.expander.min.js"></script>

  <!--Libreria para iconos-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">






<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="description" content="">
<meta name="author" content="">
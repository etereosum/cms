@extends('admin.template.main')
@section('title','Organización')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h4 style="display:inline;">Inicio</h4>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" 
            style="display:inline;float:right;">
            Volver
        </a>
		<hr>
    </div>
    <div class="row">
        <div class="col-md-8">
            <center>
                <img src="{{ asset('imagenes/esquema.jpg') }}" width="80%">
            </center>

        </div>
        <div class="col-md-4">
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    Opciones
                </a>
                <a href="{{ route('banner.index') }}" class="list-group-item list-group-item-action">Banner principal</a>
                <a href="{{ route('noticias.index') }}" class="list-group-item list-group-item-action">Noticias</a>
                <a href="{{ route('links.index') }}" class="list-group-item list-group-item-action">Links de interes</a>
                <a href="{{ route('empresa.show',1) }}" class="list-group-item list-group-item-action">Información de la organización</a>
            </div>
        </div>

    </div>

  </div>
</div>

@endsection
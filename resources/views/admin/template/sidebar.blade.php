    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">Panel Administrativo</div>
      <div class="list-group list-group-flush">
        <a href="{{ route('dash.index') }}" class="list-group-item list-group-item-action bg-light">Inicio</a>
        <a href="{{ route('noticias.index') }}" class="list-group-item list-group-item-action bg-light">Noticias</a>
        <a href="{{ route('empresa.show',1) }}" class="list-group-item list-group-item-action bg-light">Organización</a>
        <a href="{{ route('users.index') }}" class="list-group-item list-group-item-action bg-light">Usuarios</a>
        <a href="{{ route('quienes_somos.index') }}" class="list-group-item list-group-item-action bg-light">Quiénes somos</a>
        <a href="{{ route('junta.index') }}" class="list-group-item list-group-item-action bg-light">Junta directiva</a>
        <a href="{{ route('normativa.index') }}" class="list-group-item list-group-item-action bg-light">Normativa</a>
        <a href="{{ route('correos.index') }}" class="list-group-item list-group-item-action bg-light">Correos</a>
        <a href="{{ route('otros.index') }}" class="list-group-item list-group-item-action bg-light">Otros</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->
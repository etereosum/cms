<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title','Ingrese title') </title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('admin/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('admin/css/simple-sidebar.css')}}" rel="stylesheet">

  <link href="{{ asset('admin/data_tables/datatables.min.css') }}" rel="stylesheet">
  

</head>
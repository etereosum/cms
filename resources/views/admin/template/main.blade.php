<!DOCTYPE html>
<html lang="en">

@include('admin.template.header')
@include('admin.template.js')

<body>

  <div class="d-flex" id="wrapper">

    @include('admin.template.sidebar')


    <div id="page-content-wrapper">

      @include('admin.template.navbar')

      @include('admin.template.content')

    </div>


  </div>
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>


</html>

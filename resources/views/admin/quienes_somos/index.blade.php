@extends('admin.template.main')
@section('title','Quiénes somos')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h4 style="display:contents;display:inline;">Quiénes somos</h4>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" 
            style="display:inline;float:right;">
            Volver
        </a>
                
		<hr>
    </div>

    <div class="container">
        @include('librerias.error')
        
        <div class="row">

          @include('admin.quienes_somos.historia.edit')

          @include('admin.quienes_somos.horizonte.edit')

          @include('admin.quienes_somos.estatutos.edit')


        </div>
    </div>

  </div>
</div>

@endsection
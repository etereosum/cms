<div class="col-md-12">
<div class="card">
    <div class="card-body">
    <h5>Historia de la organización</h5>
    <hr>
    <form method="POST" action="{{route('historia.update',$historia)}}" enctype="multipart/form-data">

        @csrf
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label for="">Titulo *</label>
            <input type="text" class="form-control-file" name="titulo" value="{{ $historia->titulo }}">
        </div>

        <div class="form-group">
            @if($historia->img_ruta)
                <img src="{{asset($historia->img_ruta)}}" width="400">
                <br><br>
				<dd> {{ $historia->img_name }}
					<a href="{{ route('historia.delete_img',$historia->id) }}" 
						onclick="return confirm('¿Esta seguro de borra la imagen?')"
						class="btn btn-danger btn-sm">Eliminar</a>
				</dd>
            @endif	
            <br>
            <input type="file" class="form-control-file" name="img_file">
        </div>

        <div class="form-group">
            <label for="">Texto historia *</label>
            <textarea type="text" class="form-control form-control-sm" rows="6"
            name="texto_historia">{{ $historia->texto }}</textarea>
            <small id="emailHelp" class="form-text text-muted">Escriba la reseña historica.</small>
        </div>

        <button type="submit" class="btn btn-primary">Guardar cambios en historia</button>

    </form>
    </div>
</div>
</div>

<script>
    CKEDITOR.replace( 'texto_historia' );
</script>
<div class="col-md-12" style="margin-top:10px;">
<div class="card">
    <div class="card-body">
    <h5>Horizonte institucional</h5>
    <hr>
    <form method="POST" action="{{route('horizonte.update',$horizonte)}}" enctype="multipart/form-data">

        @csrf
        <input type="hidden" name="_method" value="PUT">

        <div class="form-group">
            <label for="">Titulo *</label>
            <input type="text" class="form-control-file" name="titulo" value="{{ $horizonte->titulo }}">
        </div>

        <div class="form-group">
            <label for="">Horizonte institucional *</label>
            <textarea type="text" class="form-control form-control-sm" rows="6"
            name="texto_horizonte">{{ $horizonte->texto }}</textarea>
            <small class="form-text text-muted">Escriba los fines y objetivos institucionales.</small>
        </div>
        <button type="submit" class="btn btn-primary">Guardar cambios en horizonte institucional</button>

    </form>
    </div>
</div>
</div>

<script>
    CKEDITOR.replace( 'texto_horizonte' );
</script>
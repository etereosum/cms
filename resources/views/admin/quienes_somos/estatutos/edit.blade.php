<div class="col-md-12" style="margin-top:10px;">
<div class="card">
    <div class="card-body">
    <h5>Estatutos</h5>
    <hr>
    <form method="POST" action="{{route('estatutos.update',$estatutos)}}" enctype="multipart/form-data">

        @csrf
        <input type="hidden" name="_method" value="PUT">

        <div class="form-group">
            <label for="">Titulo *</label>
            <input type="text" class="form-control-file" name="titulo" value="{{ $estatutos->titulo }}">
        </div>

        <div class="form-group">
            @if($estatutos->img_ruta)
                <img src="{{asset($estatutos->img_ruta)}}" width="400">
                <br><br>
				<dd> {{ $estatutos->img_name }}
					<a href="{{ route('estatutos.delete_img') }}" 
						onclick="return confirm('¿Esta seguro de borra la imagen?')"
						class="btn btn-danger btn-sm">Eliminar</a>
				</dd>
                <br>
            @endif	
            <input type="file" class="form-control-file" name="img_file">
        </div>

        <div class="form-group">
            <label for="">Texto estatutos *</label>
            <textarea type="text" class="form-control form-control-sm" rows="3"
            name="texto_estatutos">{{ $estatutos->texto }}</textarea>
            <small class="form-text text-muted">Escriba los estatutos.</small>
        </div>

        <div class="form-group">
            @if($estatutos->doc_ruta)
					<label for="">Archivo adjunto: 
					<a href="{{ route('document-estatutos.show',[$estatutos->id,$estatutos->doc_name]) }}" target="_blank">
							{{ $estatutos->doc_name }}</a></label>

					<a href="{{ route('estatutos.delete-pos',$estatutos->id) }}" 
						onclick="return confirm('¿Esta seguro de borra el documento?')"
						class="btn btn-danger btn-sm">Eliminar</a>
				@endif
				<input type="file" class="form-control-file" name="doc_file">
		</div>

        <button type="submit" class="btn btn-primary">Guardar cambios en estatutos</button>

    </form>
    </div>
</div>
</div>


<script>
    CKEDITOR.replace( 'texto_estatutos' );
</script>
@extends('admin.template.main')
@section('title','Noticias')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h4 style="display:inline;">Noticias</h4>
				<a href="javascript:window.history.back();"class="btn btn-success btn-sm" 
					style="display:inline;float:right;">
            Volver
        </a>
		<hr>
    </div>
    
	<div class="col-md-8">
		@include('librerias.error')

		<form method="POST" action="{{route('noticias.update',$noticia)}}" enctype="multipart/form-data">

		<input type="hidden" name="_method" value="PUT">

		@csrf

		<div class="form-group">
			<label for="">Titulo *</label>
			<input type="text" value="{{$noticia->titulo}}" class="form-control" 
						 name="titulo" aria-describedby="Titulo" placeholder="Mi nueva noticia">
			<small name="titulo" class="form-text text-muted">Escriba un titulo para su noticia.</small>
		</div>
		<div class="form-group">

			@if($noticia->img_ruta)
				<img src="{{asset($noticia->img_ruta)}}" width="400">
				<br><br>
				<dd> {{ $noticia->img_name }}
					<a href="{{ route('noticia.delete_img',$noticia->id) }}" 
						onclick="return confirm('¿Esta seguro de borra el documento?')"
						class="btn btn-danger btn-sm">Eliminar</a>
				</dd>

			@endif
	
			<label for="">Imagen</label>
    		<input type="file" class="form-control-file" name="img_file">
		</div>
		<div class="form-group">
			<label for="">Contenido</label>
			<textarea class="form-control" name="texto" id="" cols="30" rows="10">{{ $noticia->texto }}</textarea>
			<small id="" class="form-text text-muted">Ingrese el texto de su noticia.</small>
		</div>
		
		<div class="form-group">
			@if($noticia->doc_ruta)
					<label for="">Archivo adjunto: 
					<a href="{{ route('document-noticia.show',[$noticia->id,$noticia->doc_name]) }}" target="_bñank">
							{{ $noticia->doc_name }}</a></label>

					<a href="{{ route('noticia.delete-pos',$noticia->id) }}" 
						onclick="return confirm('¿Esta seguro de borra el documento?')"
						class="btn btn-danger btn-sm">Eliminar</a>
				@endif

				<input type="file" class="form-control-file" name="doc_file">
		</div>

		<button type="submit" class="btn btn-primary">Guardar cambios</button>

		</div>
		</form>
	</div>

  </div>
</div>

<script>
    CKEDITOR.replace( 'texto' );
</script>

@endsection
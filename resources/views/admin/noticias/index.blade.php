
@extends('admin.template.main')
@section('title','Noticias')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">Noticias</h5>

        <a href="{{ route('noticias.create') }}" class="btn btn-danger btn-sm float-right"
            style="display:inline; margin-left:5px;">
            Crear noticia</a>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        <p>
            @include('librerias.error')
        </p>
    </div>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Imagen</th>
                <th>Titulo</th>
                <th>Estado</th>
                <th>Creación</th>
                <th>Actualización</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($noticias as $noticia)
            <tr>
                @if($noticia->img_ruta)
                <td><img src="{{asset($noticia->img_ruta)}}" width="100"></td>
                @else
                <td>N/A</td>
                @endif
                <td>{{ $noticia->titulo }}</td>
                <td>{{ $noticia->estado }}</td>
                <td>{{ $noticia->created_at }}</td>
                <td>{{ $noticia->updated_at }}</td>
                <td>
                    <a href="{{ route('noticias.show',$noticia->id) }}" 
                            class="btn btn-outline-secondary btn-sm">Ver</a>

                    <a href="{{ route('noticias.edit',$noticia->id) }}" 
                            class="btn btn-outline-success btn-sm">Editar</a>

                    <a href="{{ route('noticias.send',$noticia->id) }}" 
                            class="btn btn-outline-primary btn-sm">Enviar</a>

                    <a href="{{ route('noticias.destroy',$noticia->id) }}" 
                            class="btn btn-outline-danger btn-sm" 
                            onclick="return confirm('¿ Esta seguro de eliminar la noticia ?')">
                        Eliminar
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

  </div>
</div>



    <script>

        $(document).ready(function() {
            $('#example').DataTable({
                "paging": true,
                "searching": true,
                "order": [[ 4, 'desc' ]]
            });
        } );


    
    </script>

@endsection


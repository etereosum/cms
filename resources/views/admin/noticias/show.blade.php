@extends('admin.template.main')
@section('title','Noticia')

@section('contenido')
<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
		<div class="ml-4 mr-4">
        <h4 style="display:inline;">Noticia</h4>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
		<hr>
		<dt style="display:inline;">Titulo:</dt>
		<dd style="display:inline;"> {{ $noticia->titulo }}</dd>
    <br><br>
    @if($noticia->img_ruta)
      <img src="{{asset($noticia->img_ruta)}}" width="500" height="300">
      <br>
      <dd> nombre: {{ $noticia->img_name }}</dd>
    @endif
		<dt>Contenido</dt>
		<dd class="text-justify"> {!! $noticia->texto !!}</dd>
    @if($noticia->doc_ruta)
		  <dt><a href="{{ route('document-noticia.show',[$noticia->id,$noticia->doc_name]) }}" target="_blank">{{ $noticia->doc_name }}</a></dt>
    @endif
		</div>
    </div>
    <br>


  </div>
</div>



    <script>

        $(document).ready(function() {
            $('#example').DataTable({
                "paging": false,
                "searching": false
            });
        } );


    
    </script>



@endsection
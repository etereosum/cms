@extends('admin.template.main')
@section('title','Noticias')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h4 style="display:contents;">Noticias</h4>
				<a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
		<hr>
    </div>
    
	<div class="col-md-8">

		@include('librerias.error')
	
		<form method="POST" action="{{route('noticias.store')}}" enctype="multipart/form-data">

		@csrf

		<div class="form-group">
			<label for="">Titulo *</label>
			<input type="text" class="form-control form-control-sm" name="titulo" 
						 aria-describedby="Titulo" placeholder="Mi nueva noticia">
			<small id="emailHelp" class="form-text text-muted">Escriba un titulo para su noticia.</small>
		</div>

		<div class="form-group">
			<label for="exampleFormControlFile1">Imagen</label>
    		<input type="file" class="form-control-file" name="img_file">
		</div>

		<div class="form-group">
			<label for="">Contenido</label>
			<textarea class="form-control" name="texto" id="" cols="30" rows="10"></textarea>
			<small id="" class="form-text text-muted">Ingrese el texto de su noticia.</small>
		</div>

		<div class="form-group">
			<label for="">Archivo adjunto</label>
    		<input type="file" class="form-control-file" name="doc_file">

		</div>

		<button type="submit" class="btn btn-primary">Crear</button>

		</div>
		</form>
	</div>

  </div>
</div>

<script>
    CKEDITOR.replace( 'texto' );
</script>

@endsection

@extends('admin.template.main')
@section('title','Otros')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">{{ $pos->titulo }}</h5>

        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" 
            style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        <p>
            @include('librerias.error')
        </p>
    </div>

		<form method="POST" action="{{ route('otros.update',$pos) }}" enctype="multipart/form-data">

		  <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="type" value="pos">

		@csrf

		<div class="form-group">
			<label for="">Titulo *</label>
			<input type="text" value="{{$pos->titulo}}" class="form-control" name="titulo">
			<small name="titulo" class="form-text text-muted">Este es el titulo.</small>
		</div>
		@if($pos->img_ruta)
			<div class="form-group">
				<img src="{{asset($pos->img_ruta)}}" width="400">
				<br>
				<dd> {{ $pos->img_name }}</dd>
				<label for="">Imagen</label>
    		<input type="file" class="form-control-file" name="img_file">
		</div>
		@endif	
		<div class="form-group">
			<label for="">Contenido</label>
			<textarea class="form-control" name="texto" id="" cols="30" rows="10">{{ $pos->texto }}</textarea>
			<small class="form-text text-muted">Ingrese una texto del plan de acción.</small>
		</div>
		
		<div class="form-group">
			@if($pos->doc_ruta)
				<label>Archivo adjunto: 
					<a href="{{ route('document-pos.show',[$pos->id, $pos->doc_name]) }}" target="_blank">
						{{ $pos->doc_name }}</a> </label>
					<a href="{{ route('otros.delete-pos',$pos->id) }}" 
					   onclick="return confirm('¿Esta seguro de borra el documento?')"
						 class="btn btn-danger btn-sm">Eliminar</a>
			@endif
					<input type="file" class="form-control-file" name="doc_file">
		</div>

		<button type="submit" class="btn btn-info">Guardar cambios</button>

		</div>
		</form>


  </div>
</div>

<script>
    CKEDITOR.replace( 'texto' );
</script>


@endsection



@extends('admin.template.main')
@section('title','Otros')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">{{ $element->nombre }}</h5>

        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        <p>
            @include('librerias.error')
        </p>
    </div>

        <form method="POST" action="{{ route('otros.update',$element) }}" enctype="multipart/form-data">

            @csrf

            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="type" value="element">

            @if($element->ruta)
                <div class="form-group">
                    <img class="img-thumbnail" src="{{asset($element->ruta)}}" width="100">
                    <br>
                    <dd> {{ $element->nombre }}</dd>
                    <label for="">Imagen</label>
                <input type="file" class="form-control-file" name="img_file">
            </div>
            @endif	


            <div class="form-group">
                <label for="">Nombre *</label>
                <input type="text" class="form-control form-control-sm" name="nombre" 
                       value="{{ $element->nombre }}">
                <small id="emailHelp" class="form-text text-muted">Escriba el nombre para este elemento.</small>
            </div>
            
            <div class="form-group">
                <label for="">Estado *</label>
                <select class="form-control form-control-sm" name="estado">
                    <option value="Activo" {{ ($element->estado == 'Activo') ? 'selected' :'' }}>
                        Activo
                    </option>
                    <option value="Inactivo" {{ ($element->estado == 'Inactivo') ? 'selected' :'' }}>
                        Inactivo
                    </option>
                </select>
                <small class="form-text text-muted">Seleccione un estado para el elemento.</small>
            </div>  

            <div class="form-group">
                <label for="">Link</label>
                <input type="text" class="form-control form-control-sm" name="link" 
                       value="{{ $element->link }}">
                <small class="form-text text-muted">Escriba el link para este elemento.</small>
            </div>

            <button type="submit" class="btn btn-primary">Guardar</button>

  </div>
</div>




@endsection


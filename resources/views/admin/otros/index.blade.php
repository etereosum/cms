
@extends('admin.template.main')
@section('title','Otros')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">Otros</h5>

        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        <p>
            @include('librerias.error')
        </p>
    </div>

    <table id="otros" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Imagen</th>
                <th>Titulo</th>
                <!-- <th>Estado</th> -->
                <th>Descripción</th>
                <th>Vinculo</th>
                <th>Doc adjunto</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
            <tr>
                @if($post->img_ruta)
                    <td><img src="{{asset($post->img_ruta)}}" width="100"></td>
                @else
                    <td>N/A</td>
                @endif

                <td>{{ $post->titulo }}</td>
                <!-- <td>{{ $post->estado }}</td> -->
                <td><pre style="height: 40px;max-width: 200px;overflow: auto;
                    font-size:10px;">{!! $post->texto !!}</pre></td>
                <td></td>
                <td><pre style="height: 40px;max-width: 100px;overflow: auto;
                    font-size:10px;">{{ $post->doc_name }}</pre></td>
                <td>
                    <a href="{{ route('otros.edit_pos',$post->id) }}" 
                            class="btn btn-outline-success btn-sm">Editar</a>

                    <!-- <a href="#" 
                            class="btn btn-outline-danger btn-sm" 
                            onclick="return confirm('¿ Esta seguro de eliminar la noticia ?')">
                        Eliminar
                    </a> -->
                </td>
            </tr>
            @endforeach
            @foreach($elements as $element)
            <tr>
                @if($element->ruta)
                    <td><img src="{{asset($element->ruta)}}" width="100"></td>
                @else
                    <td>N/A</td>
                @endif

                <td>{{ $element->nombre }}</td>
                <!-- <td>{{ $element->estado }}</td> -->
                <td></td>
                <td><pre style="height: 40px;
                                max-width: 150px;
                                overflow: auto;
                                font-size:10px;">{{ $element->link }}</pre></td>
                <td>{{ $element->doc_name }}</td>
                <td>
                    <a href="{{ route('otros.edit_element',$element->id) }}" 
                            class="btn btn-outline-success btn-sm">Editar</a>

                    <!-- <a href="#" 
                            class="btn btn-outline-danger btn-sm" 
                            onclick="return confirm('¿ Esta seguro de eliminar la noticia ?')">
                        Eliminar
                    </a> -->
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

  </div>
</div>



    <script>

        $(document).ready(function() {
            $('#example').DataTable({
                "paging": true,
                "searching": true,
                "order": [[ 4, 'desc' ]]
            });
        } );


    
    </script>

@endsection


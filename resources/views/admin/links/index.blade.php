@extends('admin.template.main')
@section('title','Links de interes')

@section('contenido')

<br>
<div class="card">
  <div class="card-body" id="links">
    <div class="card-title">
        <h4 style="display:contents;display:inline;">Links de interes</h4>
        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#create_modal"
            style="display:inline;float:right;margin-left:5px;">
            Agregar
        </button>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
                
		<hr>
    </div>

    <div class="container">
        @include('librerias.error')
        
        <div class="row">

            @foreach($imagenes as $imagen)

                <div class="col-lg-3 col-md-4 col-xs-6 thumb" style="margin-top:10px;">
                    <div class="thumbnail" href="#">
                        <a href="{{ route('banner.get_banner',$imagen->id) }}">
                        
                            <img class="img-thumbnail"
                                    src="{{asset($imagen->ruta)}}"
                                    alt="Another alt text"
                                    style="height:150px;">
                            <a href="{{ $imagen->link }}" style="font-size:9px;" target="_blank">
                                {{$imagen->link}}</a>
                        </a>
                        <div>
                             <a href="#" class="btn btn-primary btn-sm" 
                                style="font-size:.575rem;" @click="get_link({{$imagen->id}})">
                                Editar
                            </a>                   
                            <a href="{{ route('link.destroy',$imagen->id) }}" class="btn btn-dark btn-sm" 
                                style="font-size:.575rem;"
                                onclick="return confirm('¿ Esta seguro de eliminar la imagen?')">
                                Borrar
                            </a>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
    
    @include('admin.links.create_modal')
    @include('admin.links.edit_modal')
    
  </div>
</div>

<script>
    var Bus = new Vue()

    var ini = new Vue({
        el:"#links",
        data:{
            data : '',
            element : ''

        },
        methods:{
            get_link(id)
            {
                var self = this
                var route = "links/"+ id +"/edit" 
                
                axios.get(route)
                    .then( function(res){
                        if(!res.data.error){
                            self.data = res.data.dat
                            self.element = res.data.dat.element
                            // Bus.$emit('data',self.data)
                            self.action_edit()
                        }
                    })
                             
            },
            action_edit()
            {
                $("#edit_modal").modal('show')
            }
        }
    })
</script>


@endsection
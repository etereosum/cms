    <!-- Modal -->
    <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" 
        aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Agregar imagen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>

            <form   action="{{ route('links.actualizar') }}"  
                    method="POST"
                    enctype="multipart/form-data">
        <div class="modal-body">
        
                @csrf

                <div class="form-group" v-if="element">

                    <img class="img-thumbnail"
                            :src="'/'+element.ruta"
                            alt="Another alt text"
                            style="height:150px;">

                </div>

                <input type="hidden" name="id" :value="element.id">

                <div class="form-group">
                    <input type="file"  name="img_file">
                    <small class="form-text text-muted">Elija una imagen formato jpg, jpeg, png o svg</small>
                </div>

                <div class="form-group">
                    <label>Link</label>
                    <input type="text" name="link" class="form-control form-control-sm" :value="element.link">
                    <small class="form-text text-muted">Ingrese el link de la imagen</small>
                </div>

                <div class="form-group">
                    <label>Orden</label>
                    <select name="posicion" class="form-control form-control-sm">
                        
                        <option :value="i" v-for="i in data.num_elements" 
                            :selected="element.posicion == i">
                            @{{ i }}</option>
                        
                    </select>
                </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
           <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
            </form>
        </div>
    </div>
    </div>






    
    <!-- Modal -->
    <div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar imagen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <form   action="{{ route('links.store') }}"  
                    method="POST"
                    enctype="multipart/form-data">
        <div class="modal-body">

                @csrf

                <div class="form-group">
                    <input type="file"  name="img_file">
                    <small class="form-text text-muted">Elija una imagen formato jpg, jpeg, png o svg</small>
                </div>

                <div class="form-group">
                    <label>Link</label>
                    <input type="text"  name="link" class="form-control form-control-sm">
                    <small class="form-text text-muted">Ingrese el link de la imagen</small>
                </div>

                <div class="form-group">
                    <label>Orden</label>
                    <select name="posicion" class="form-control form-control-sm">
                        @foreach(range(1,$num_elements + 1) as $num)
                            <option value="{{ $num }}">{{ $num }}</option>
                        @endforeach
                    </select>
                </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
           <button type="submit" class="btn btn-primary">Agregar</button>
        </div>
            </form>
        </div>
    </div>
    </div>


@extends('admin.template.main')
@section('title','Organización')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h4 style="display:contents;display:inline;">Banner</h4>
        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#create_modal"
            style="display:inline;float:right;margin-left:5px;">
            Agregar
        </button>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
                
		<hr>
    </div>

    <div class="container">
        @include('librerias.error')
        
        <div class="row">

            @foreach($imagenes as $imagen)

                <div class="col-lg-3 col-md-4 col-xs-6 thumb" style="margin-top:10px;">
                    <div class="thumbnail" href="#">
                        <a href="{{ route('banner.get_banner',$imagen->id) }}">
                        
                            <img class="img-thumbnail"
                                    src="{{asset($imagen->ruta)}}"
                                    alt="Another alt text"
                                    style="height:150px;">

                        </a>
                        <div>
                        
                            <a href="{{ route('banner.destroy',$imagen->id) }}" class="btn btn-dark btn-sm" 
                                style="font-size:.575rem;"
                                onclick="return confirm('¿ Esta seguro de eliminar la imagen?')">
                                Borrar
                            </a>
                    
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    @include('admin.banner.create_modal')

  </div>
</div>

@endsection
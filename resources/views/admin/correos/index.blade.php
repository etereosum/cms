
@extends('admin.template.main')
@section('title','Normativa')

@section('contenido')

<br>
<div class="card">
  <div class="card-body" id="correo">
    <div class="card-title">
        <h5 style="display:inline;">Correos</h5>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
        <a href="#"class="btn btn-danger btn-sm" @click="create()"
            style="display:inline;float:right;margin-right:5px;">
            Agregar
        </a>
        <hr>
        <p>
            @include('librerias.error')
            <div class="alert alert-danger" role="alert" v-if="errors">
                <ul v-for="error in errors">
                    <li v-for="e in error" v-text="e"></li>
                </ul>
            </div>
        </p>
    </div>
    

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label v-if="estado == 'crear'" style="color:blue;"> Agregar Correo</label>
                            <label v-else style="color:red;"> Editar Correo</label>
                            <input type="email" name="correo" class="form-control form-control-sm" v-model="correo">
                            <small v-if="estado == 'crear'" class="form-text text-muted">Ingrese un correo electrónico</small>
                            <small v-else class="form-text text-muted">Modifique el correo electrónico</small>
                        </div>
                        <div class="form-group">
                            <a href="#" class="btn btn-success btn-sm" @click="store()" v-if="estado == 'crear'">Agregar</a>
                            <a href="#" class="btn btn-info btn-sm" @click="update()" v-else>Guardar cambios</a>
                        </div>
                    </div>
                </div>
                <!-- <div class="card">
                    <div class="card-body">

                        <form action="#" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Agregar masivamente</label>
                                <input type="file"  name="file">
                                <small class="form-text text-muted">Ingrese listado en formato  xls</small>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info btn-sm">Aceptar</button>
                                <a href="#" class="btn btn-warning btn-sm">Descargar plantilla</a>
                            </div>
                        </form>

                    </div>
                </div> -->
            </div>
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" v-model="elemento">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" @click="search()">Buscar</button>
                    </div>
                </div>
                <table class="table table-striped" style="font-size:11px;">
                    <thead>
                        <tr>
                            <th>Correo</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="correo in correos">                        
                            <td v-text="correo.correo"></td>
                            <td>
                                <a href="#" @click="edit(correo)"
                                    class="btn btn-outline-success btn-sm">Editar</a>

                                 <a href="#" @click="drop(correo.id)"
                                    class="btn btn-outline-danger btn-sm">Eliminar</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>



  </div>
</div>



<script>

var obj = new Vue({
    el: "#correo",
    data: {
        correo  : '',
        id      : '',
        correos : '',
        estado  : '',
        errors  : '',
        elemento: ''
    },
    methods:{
        store(){
            var self = this
            if(this.correo){
                axios.post('/admin/correos', { correo: this.correo })
                    .then(function(res){
                        console.log('store: ', res);
                        
                        if(res.data.error){
                            self.errors = res.data.message
                        } else {
                            self.get_correos()
                        }
                    })
            }
            else {
                this.errors = [['El correo es requerido']]
            }
        },
        edit(correo){
            this.estado = 'editar'
            this.correo = correo.correo 
            this.id = correo.id
        },
        create(){
            this.estado = 'crear'
            this.correo = ''
            this.id = ''
            this.errors = ''
        },
        update(){
            var self = this
            if(this.correo){
                axios.put('/admin/correos/'+this.id, { correo: this.correo })
                    .then(function(res){
                        console.log(res);
                        if(res.data.error) {
                            self.errors = res.data.message
                        } else {
                            self.correo = ''
                            self.get_correos()
                        }
                    })
            }
            else {
                this.errors = [['El correo es requerido']]
            }
        },//.update
        drop(correo_id){
            var self = this
            if(confirm('¿Desea eliminar el registro?')){
                axios.get('/admin/correos/destroy/'+correo_id)
                    .then(function(res){
                        if(res.data.error){
                            alert(res.data.message)
                        } else {
                            alert(res.data.message)
                            self.get_correos()
                        }
                    })
            }
        },
        search(){
            var self = this
            var route = '/admin/correos/search/'+this.elemento

            axios.get(route)
                .then(function(res){
                    console.log('res',res);
                    if(res.data.error) {
                        alert(res.data.message)
                    } else {
                        self.correos = []
                        self.correos = res.data.dat
                    }
                })
        },
        get_correos(){
            var self = this
            axios.get('/admin/correos/list')
                .then(function(res){
                    console.log('get_correos ',res);
                    
                    if(!res.data.error) {
                        self.correos = res.data
                    }
                })
        }
    },
    created(){
        this.get_correos();
        this.estado = 'crear'

    }

})


</script>

@endsection


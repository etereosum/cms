
@extends('admin.template.main')
@section('title','Noticias')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">Usuarios del sistema</h5>

        <a href="javascript:window.history.back();" class="btn btn-success btn-sm" 
           style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        <p>
            @include('librerias.error')
        </p>
    </div>

    <div id="user_edit">

        <form method="POST" action="{{route('users.update',$user)}}">
            @csrf
            <input type="hidden" name="_method" value="PUT">

            <div class="form-group">
                <label>Nombres *:</label>
                <input type="text" name="nombres"
                    value="{{ $user->nombres }}" class="form-control form-control-sm">
            </div>

            <div class="form-group">
                <label>Apellidos *:</label>
                <input placeholder="apellidos" type="text" name="apellidos"
                    value="{{ $user->apellidos }}" class="form-control form-control-sm">
            </div>

            <div class="form-group">
                <label>Teléfono</label>
                <input type="text" name="telefono" class="form-control form-control-sm" 
                       value="{{ $user->telefono }}">
            </div>

            <div class="form-group">
                <label>Institución :</label>
                <input type="text" name="institucion" 
                    value="{{ $user->institucion }}" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Estado</label>
                <select name="estado" class="form-control form-control-sm">
                    <option value="Activo" {{ ( $user->estado == 'Activo') ? 'selected' :'' }}>
                        Activo</option>
                    <option value="Inactivo" {{ ( $user->estado == 'Inactivo') ? 'selected' :'' }}>
                        Inactivo</option>
                </select>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control form-control-sm" 
                       value="{{ $user->email }}">
            </div>

            <button type="submit" class="btn btn-primary">Guardar</button>

        </form>

    </div>


  </div>
</div>

@endsection


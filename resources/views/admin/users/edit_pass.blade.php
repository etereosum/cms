
@extends('admin.template.main')
@section('title','Noticias')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">Usuarios del sistema</h5>

        <a href="javascript:window.history.back();" class="btn btn-success btn-sm" 
                    style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        <p>
            @include('librerias.error')
        </p>
    </div>

    <div id="pass_update" class="col-md-8" style="font-size:12px;">

        <form method="POST" action="{{route('users.update_pass')}}">
            
            @csrf

            <input type="hidden" name="user_id" value="{{ $id }}">

            <div class="form-group">
                <label>Nueva contraseña *:</label>
                <input type="password" name="password"
                       v-model="password"
                       class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Confirme contraseña</label>
                <input type="password" name="confirm" class="form-control form-control-sm"
                       v-model="confirm"
                       @keyup="password_validate()">
            </div>
        
            <button type="submit" class="btn btn-primary" 
                v-if="show_btn_submit">Guardar nueva contraseña</button>

        </form>

    </div>
  </div>
</div>

<script>
    new Vue({
        el: "#pass_update",
        data:{
            password : '',
            confirm  : '',
            show_btn_submit : false
        },
        methods:{
            password_validate(){
                if(this.password == this.confirm){
                    this.show_btn_submit = true
                } else {
                    this.show_btn_submit = false
                }
            }
        }
    })
</script>

@endsection



@extends('admin.template.main')
@section('title','Noticias')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">User</h5>

        <a href="{{ route('users.create') }}" class="btn btn-danger btn-sm float-right"
            style="display:inline; margin-left:5px;">
            Crear user</a>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        <p>
            @include('librerias.error')
        </p>
    </div>

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Estado</th>
                <th>Teléfono</th>
                <th>Institución</th>
                <th>Email</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->nombres. ' ' . $user->apellidos }}</td>
                    <td>{{ $user->estado }}</td>
                    <td>{{ $user->telefono }}</td>
                    <td>{{ $user->institucion }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <a href="{{ route('users.edit',$user->id) }}"
                            class="btn btn-outline-success btn-sm">Editar</a>

                        <a href="{{ route('users.edit_pass',$user->id) }}"
                            class="btn btn-outline-secondary btn-sm">Contraseña</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>


  </div>
</div>



    <script>

        $(document).ready(function() {
            $('#example').DataTable({
                "paging": true,
                "searching": true,
                "order": [[ 4, 'desc' ]]
            });
        } );


    
    </script>

@endsection







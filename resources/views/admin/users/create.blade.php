
@extends('admin.template.main')
@section('title','Noticias')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">Usuarios del sistema</h5>

        <a href="javascript:window.history.back();" class="btn btn-success btn-sm" 
                    style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        <p>
            @include('librerias.error')
        </p>
    </div>

    <div id="user_create" class="col-md-8" style="font-size:12px;">

        <form method="POST" action="{{route('users.store')}}">
            @csrf
            <div class="form-group">
                <label>Nombres *:</label>
                <input type="text" name="nombres" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Apellidos *:</label>
                <input type="text" name="apellidos"
                    class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Teléfono</label>
                <input type="text" name="telefono" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Institución :</label>
                <input type="text" name="institucion" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Email *:</label>
                <input type="email" name="email" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Contraseña *:</label>
                <input type="password" name="password" class="form-control form-control-sm"
                    v-model="password" @keyup="validate()">
            </div>
            <div class="form-group">
                <label>Confirme contraseña *:</label>
                <input type="password" class="form-control form-control-sm" 
                    @keyup="password_validate()"
                    v-model="confirm">
                <small class="form-text text-muted" v-if="show_btn_submit">
                    Contraseña confirmada</small>
            </div>

            <button type="submit" class="btn btn-primary" 
                v-if="show_btn_submit">Guardar</button>

        </form>

    </div>
  </div>
</div>

<script>
    new Vue({
        el: "#user_create",
        data:{
            password : '',
            confirm  : '',
            show_btn_submit : false
        },
        methods:{
            password_validate(){
                if(this.password == this.confirm){
                    this.show_btn_submit = true
                } else {
                    this.show_btn_submit = false
                }
            }
        }
    })
</script>

@endsection


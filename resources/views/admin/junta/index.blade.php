@extends('admin.template.main')
@section('title','Junta')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">Junta directiva</h5>
        <a href="{{ route('junta.create') }}" class="btn btn-danger btn-sm float-right"
            style="display:inline; margin-left:5px;">
            Agregar integrante</a>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" 
            style="display:inline;float:right;">
            Volver
        </a>
      <br><br>
        @include('librerias.error')
      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Cargo</th>
                <th>Institución</th>
                <th>telefono</th>
                <th>email</th>
                <th>Estado</th>
                <th>Posicion</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($junta as $integrante)
              @if($integrante->estado == 'Inactivo')
                <tr style="background:#FEE9E5;">
              @else
                <tr>
              @endif
                <td>{{ $integrante->nombres.' '.$integrante->apellidos }}</td>
                <td>{{ $integrante->cargo->nombre }}</td>
                <td>{{ $integrante->institucion }}</td>
                <td>{{ $integrante->telefono }}</td>
                <td>{{ $integrante->email }}</td>
                <td>{{ $integrante->estado }}</td>
                <td>{{ $integrante->posicion }}</td>
                <td>
                    <a href="{{ route('junta.show',$integrante->id) }}" 
                            class="btn btn-outline-secondary btn-sm">Ver</a>

                    <a href="{{ route('junta.edit',$integrante->id) }}" 
                            class="btn btn-outline-success btn-sm">Editar</a>

                    <a href="{{ route('junta.destroy',$integrante->id) }}" 
                            class="btn btn-outline-danger btn-sm" 
                            onclick="return confirm('¿ Esta seguro de eliminar el integrante ?')">
                        Eliminar
                    </a>

                </td>
    
              </tr>
            @endforeach
        </tbody>
      </table>

    </div>

      <hr>


  </div>
</div>


@endsection
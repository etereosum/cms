@extends('admin.template.main')
@section('title','Junta directiva')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h4 style="display:inline;">Integrante junta directiva</h4>
				<a href="javascript:window.history.back();"class="btn btn-success btn-sm" 
					style="display:inline;float:right;">
            Volver
        </a>
		<hr>
    </div>
    
	<div class="col-md-8">
		@include('librerias.error')

		<form method="POST" action="{{route('junta.update',$integrante)}}">

		<input type="hidden" name="_method" value="PUT">

        @csrf

		<div class="form-group">
			<label for="">Nombres *</label>
            <input type="text" value="{{$integrante->nombres}}" class="form-control form-control-sm" 
                name="nombres" aria-describedby="Titulo" placeholder="Mi nueva noticia">
			<small name="" class="form-text text-muted">Escriba el nombre completo.</small>
        </div>
		<div class="form-group">
			<label for="">Apellidos *</label>
            <input type="text" value="{{$integrante->apellidos}}" class="form-control form-control-sm" 
                name="apellidos" aria-describedby="Titulo" placeholder="Mi nueva noticia">
			<small name="" class="form-text text-muted">Escriba los apellido.</small>
        </div>
		<div class="form-group">
			<label for="">Teléfono</label>
            <input type="text" value="{{$integrante->telefono}}" class="form-control form-control-sm" 
                name="telefono" aria-describedby="Titulo" placeholder="Mi nueva noticia">
			<small name="" class="form-text text-muted">Escriba los apellido.</small>
        </div>
        
		<div class="form-group">
			<label for="">Email</label>
            <input type="email" value="{{$integrante->email}}" class="form-control form-control-sm" name="email" 
                aria-describedby="Titulo" placeholder="Mi nueva noticia">
			<small name="" class="form-text text-muted">Escriba el email.</small>
        </div>
        <div class="form-group">
			<label for="">Cargo</label>
            <select name="cargo_id" class="form-control form-control-sm">
                @foreach($cargos as $cargo)
                    <option value="{{ $cargo->id }}" {{ ( $cargo->id == $integrante->cargo_id) ? 'selected' :'' }}>{{ $cargo->nombre }}</option>
                @endforeach
            </select>
			<small name="" class="form-text text-muted">Seleccione un cargo, ej: Presidente.</small>
        </div>

        <div class="form-group">
			<label for="">Institución</label>
            <input type="text" class="form-control form-control-sm" value="{{$integrante->institucion}}"
                name="institucion" aria-describedby="Titulo">
			<small name="" class="form-text text-muted">Escriba el nombre de la institución a la que pertenece.</small>
        </div>

        <div class="form-group">
			<label for="">Estado</label>
            <select name="estado" class="form-control form-control-sm">
                @foreach(['Activo','Inactivo'] as $estado)
                    <option value="{{ $estado }}" {{ ( $estado == $integrante->estado) ? 'selected' :'' }}>{{ $estado }}</option>
                @endforeach
            </select>
			<small>
        </div>
        <div class="form-group">
			<label for="">Posición</label>
            <select name="position" class="form-control form-control-sm">
                @php
                    $count = 1
                @endphp
                <option disabled selected>- -</option>
                <option value="1">Al comienzo</option>
                @foreach($integrantes as $int)
                    @if($integrante->id != $int->id)
                    <option value="{{$int->posicion}}">
                        {{ 'Despues de: ('.$int->cargo->nombre.') '.$int->nombres.
                        ' '.$int->apellidos}}</option>
                    @endif
                @endforeach
            </select>
			<small name="" class="form-text text-muted">Seleccione la posición.</small>
        </div>
		<button type="submit" class="btn btn-primary">Editar</button>

		</div>
		</form>
	</div>

  </div>
</div>

@endsection
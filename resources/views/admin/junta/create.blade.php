@extends('admin.template.main')
@section('title','Junta directiva')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h4 style="display:inline;">Integrante junta directiva</h4>
				<a href="javascript:window.history.back();"class="btn btn-success btn-sm" 
					style="display:inline;float:right;">
            Volver
        </a>
		<hr>
    </div>
    
	<div class="col-md-8">
		@include('librerias.error')

		<form method="POST" action="{{route('junta.store')}}">

        @csrf

		<div class="form-group">
			<label for="">Nombres *</label>
            <input type="text" value="" class="form-control form-control-sm" 
                name="nombres" aria-describedby="Titulo">
			<small name="" class="form-text text-muted">Escriba el nombre completo.</small>
        </div>
		<div class="form-group">
			<label for="">Apellidos *</label>
            <input type="text" value="" class="form-control form-control-sm" 
                name="apellidos" aria-describedby="Titulo">
			<small name="" class="form-text text-muted">Escriba los apellido.</small>
        </div>
		<div class="form-group">
			<label for="">Teléfono</label>
            <input type="text" value="" class="form-control form-control-sm" 
                name="telefono" aria-describedby="Titulo">
			<small name="" class="form-text text-muted">Escriba los apellido.</small>
        </div>
        
		<div class="form-group">
			<label for="">Email</label>
            <input type="email" value="" class="form-control form-control-sm" name="email" 
                aria-describedby="Titulo">
			<small name="" class="form-text text-muted">Escriba el email.</small>
        </div>
        <div class="form-group">
			<label for="">Cargo</label>
            <select name="cargo_id" class="form-control form-control-sm">
                <option selected disabled></option>
                @foreach($cargos as $cargo)
                    <option value="{{ $cargo->id }}">{{ $cargo->nombre }}</option>
                @endforeach
            </select>
			<small name="" class="form-text text-muted">Seleccione un cargo, ej: Presidente.</small>
        </div>
        
        <div class="form-group">
			<label for="">Institución</label>
            <input type="text" value="" class="form-control form-control-sm" 
                name="institucion" aria-describedby="Titulo" >
			<small name="" class="form-text text-muted">Escriba el nombre de la institución a la que pertenece.</small>
        </div>
        <div class="form-group">
			<label for="">Posición</label>
            <select name="position" class="form-control form-control-sm">
                @php
                    $count = 1
                @endphp
                <option value="{{$count}}">Al comienzo</option>
                @foreach($integrantes as $integrante)
                    <option value="{{++$count}}">{{ 'Despues de: ('.$integrante->cargo->nombre.') '.$integrante->nombres.
                        ' '.$integrante->apellidos}}</option>
                @endforeach
            </select>
			<small name="" class="form-text text-muted">Seleccione la posición.</small>
        </div>



		<button type="submit" class="btn btn-primary">Crear</button>

		</div>
		</form>
	</div>

  </div>
</div>

@endsection
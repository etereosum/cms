@extends('admin.template.main')
@section('title','Junta')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">Junta directiva</h5>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" 
            style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        @include('librerias.error')

        <div class="row">
        
        <div class="col-md-6">
            <dt>Nombre:</dt>    
            <dd>{{ $integrante->nombres .' '. $integrante->apellidos }}</dd>

            <hr>

            <dt>Telefono:</dt>
            <dd>{{ $integrante->telefono }}</dd>

            <hr>

            <dt>Institucion:</dt>
            <dd>{{ $integrante->institucion }}</dd>
            
            <hr>
            <dt>Creó:</dt>
            <dd>{{ $integrante->user_create->nombres .' '. $integrante->user_create->apellidos}}
                {{ $integrante->created_at }}
            </dd>

        </div>

        <div class="col-md-6">
            <dt>Cargo:</dt>
            <dd>{{ $integrante->cargo->nombre }}</dd>

            <hr>

            <dt>Email:</dt>
            <dd>{{ $integrante->email }}</dd>

            <hr>

            <dt>Estado:</dt>
            <dd>{{ $integrante->estado }}</dd>

            <hr>
            <dt>Creó:</dt>
            <dd>{{ $integrante->user_update->nombres .' '. $integrante->user_update->apellidos}}
                {{ $integrante->updated_at }}
            </dd>

        </div>

        </div>

    </div>


  </div>
</div>


@endsection

@extends('admin.template.main')
@section('title','Normativa')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">Normativa</h5>

        <a href="{{ route('normativa.create') }}" class="btn btn-danger btn-sm float-right"
            style="display:inline; margin-left:5px;">
            Agregar normativa</a>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
        <hr>
        <p>
            @include('librerias.error')
        </p>
    </div>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Imagen  </th>
                <th>Titulo  </th>
                <th>Estado  </th>
                <th>Descripción</th>
                <th>Fecha   </th>
                <th>Creó    </th>
                <th>Acción  </th>
            </tr>
        </thead>
        <tbody>
            @foreach($normativas as $normativa)
            <tr>
                <td><img src="{{asset($normativa->img_ruta)}}" width="100"></td>
                <td>{{ $normativa->titulo }}</td>
                <td>{{ $normativa->estado }}</td>
                <td style="font-size:10px;">{{ $normativa->texto }}</td>
                <td>{{ $normativa->created_at }}</td>
                <td>{{ $normativa->user_create->nombres .' '.
                       $normativa->user_create->apellidos}}</td>
                <td>
                    @if($normativa->doc_name)
                        <a href="{{ route('document-normativa.show',[$normativa->id,$normativa->doc_name]) }}" 
                                class="btn btn-outline-secondary btn-sm" target="–blanck">Ver doc</a>
                    @endif
                    <a href="{{ route('normativa.edit',$normativa->id) }}" 
                            class="btn btn-outline-success btn-sm">Editar</a>

                    <a href="{{ route('normativa.destroy',$normativa->id) }}" 
                            class="btn btn-outline-danger btn-sm" 
                            onclick="return confirm('¿ Esta seguro de eliminar la normativa ?')">
                        Eliminar
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

  </div>
</div>



    <script>

        $(document).ready(function() {
            $('#example').DataTable({
                "paging": true,
                "searching": true,
                "order": [[ 4, 'desc' ]]
            });
        } );


    
    </script>

@endsection


@extends('admin.template.main')
@section('title','Normativa')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h4 style="display:contents;">Normativa</h4>
				<a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
		<hr>
    </div>
    
	<div class="col-md-8">

		@include('librerias.error')
	
		<form method="POST" action="{{route('normativa.update',$normativa)}}" enctype="multipart/form-data">

        <input type="hidden" name="_method" value="PUT">
		@csrf

		<div class="form-group">
			<label for="">Titulo *</label>
			<input type="text" class="form-control form-control-sm" name="titulo" 
						 aria-describedby="Titulo" placeholder="Nueva normativa"
                         value="{{ $normativa->titulo }}">
			<small id="emailHelp" class="form-text text-muted">Escriba un titulo para normativa.</small>
		</div>

		<div class="form-group">
			@if($normativa->img_ruta)
				<img src="{{asset($normativa->img_ruta)}}" width="400">
				<br><br>
				<dd> {{ $normativa->img_name }}
					<a href="{{ route('normativa.delete_img',$normativa->id) }}" 
						onclick="return confirm('¿Esta seguro de borra el documento?')"
						class="btn btn-danger btn-sm">Eliminar</a>
				</dd>

			@endif	
				<label for="">Agregar imagen</label>
				<input type="file" class="form-control-file" name="img_file">
				<hr>
		</div>



		<div class="form-group">
			<label for="">Descripción</label>
			<textarea class="form-control" name="texto" id="" cols="30" rows="4"
                >{{ $normativa->texto }}</textarea>
			<small id="" class="form-text text-muted">Ingrese una descripcion de la normativa.</small>
		</div>

		<hr>

		<div class="form-group">
				@if($normativa->doc_ruta)
					<label for="">Archivo adjunto: 
					<a href="{{ route('document-normativa.show',[$normativa->id,$normativa->doc_name]) }}">
							{{ $normativa->doc_name }}</a></label>

					<a href="{{ route('normativa.delete-pos',$normativa->id) }}" 
						onclick="return confirm('¿Esta seguro de borra el documento?')"
						class="btn btn-danger btn-sm">Eliminar</a>
				@endif
			<input type="file" class="form-control-file" name="doc_file">
		</div>

		<hr>

		<button type="submit" class="btn btn-primary">Guardar cambios</button>

		</div>
		</form>
	</div>

  </div>
</div>

@endsection
@extends('admin.template.main')
@section('title','Organización')

@section('contenido')

<br>
<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h5 style="display:inline;">Organización</h5>
        <a href="javascript:window.history.back();"class="btn btn-success btn-sm" 
            style="display:inline;float:right;">
            Volver
        </a>
        @include('librerias.error')

    </div>

      <hr>
    <div class="row">
    
      <div class="col-md-6">
        <dt>Nombre:</dt>    
        <dd>{{ $empresa->nombre }}</dd>

        <hr>

        <dt>Teléfono 1:</dt>
        <dd>{{ $empresa->telefono_1 }}</dd>

        <hr>
        <dt>Celular 1:</dt>
        <dd>{{ $empresa->celular_1 }}</dd>

        <hr>

        <dt>Dirección:</dt>
        <dd>{{ $empresa->direccion }}</dd>

        <hr>

        <dt>Horario de atención:</dt>
        <dd>{{ $empresa->horario }}</dd>
        
      </div>

      <div class="col-md-6">
        <dt>Nit</dt>
        <dd>{{ $empresa->nit }}</dd>

        <hr>

        <dt>Teléfono 2:</dt>
        <dd>{{ $empresa->telefono_1 }}</dd>

        <hr>

        <dt>Celular 2:</dt>
        <dd>{{ $empresa->celular_2 }}</dd>

        <hr>

        <dt>Email:</dt>
        <dd>{{ $empresa->email }}</dd>

        <hr>
        <a href="{{ route('empresa.edit',1) }}" class="btn btn-danger">Editar organización</a>
        
      </div>

    </div>

  </div>
</div>


@endsection
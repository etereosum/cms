
@extends('admin.template.main')
@section('title','Organización')

@section('contenido')

<br>

<div class="card">
  <div class="card-body">
    <div class="card-title">
        <h4 style="display:contents;">Organización</h4>
		<a href="javascript:window.history.back();"class="btn btn-success btn-sm" style="display:inline;float:right;">
            Volver
        </a>
		<hr>
    </div>
    	@include('librerias.error')
		<form method="POST" action="{{ route('empresa.update',$empresa) }}">
		        <input type="hidden" name="_method" value="PUT">
				@csrf
    		<div class="row" >
				<div class="col-md-6"><!----------------------Inicio columna Izquierda----------------------------->
					<div class="form-group">
						<label for="">Nombre *</label>
						<input type="text" class="form-control form-control-sm" name="nombre" 
									 aria-describedby="Titulo" value="{{ $empresa->nombre }}">
						<small class="form-text text-muted">Escriba el nombre de la organización (requerido).</small>
					</div>

			        <div class="form-group">
						<label for="">Teléfono 1 *</label>
						<input type="text" class="form-control form-control-sm" name="telefono_1" 
									 aria-describedby="Titulo" value="{{ $empresa->telefono_1 }}">
						<small class="form-text text-muted">Escriba un teléfono fijo. Ej: (6) 3333333. (requerido).</small>
					</div>

					 <div class="form-group">
						<label for="">Celular 1 *</label>
						<input type="text" class="form-control form-control-sm" name="celular_1" 
									 aria-describedby="Titulo" value="{{ $empresa->celular_1 }}">
						<small class="form-text text-muted">Escriba un teléfono celular (requerido).</small>
					</div>

					<div class="form-group">
						<label for="">Dirección *</label>
						<input type="text" class="form-control form-control-sm" name="direccion" 
									 aria-describedby="Titulo" value="{{ $empresa->direccion }}">
						<small class="form-text text-muted">Escriba la dirección de la organización.</small>
					</div>  
					<button type="submit" class="btn btn-info">Guardar Cambios</button>
			       
				</div><!----------------------Fin columna Izquierda----------------------------->

				<!---------------------------- Inicio columna derecha--------------------------->
				<div class="col-md-6">
					 <div class="form-group">
						<label for="">Nit</label>
						<input type="text" class="form-control form-control-sm" name="nit" 
									 aria-describedby="Titulo" value="{{ $empresa->nit }}">
						<small class="form-text text-muted">Escriba el nit de la organización (opcional).</small>
					</div>

					 <div class="form-group">
						<label for="">Teléfono 2</label>
						<input type="text" class="form-control form-control-sm" name="telefono_2" 
									 aria-describedby="Titulo" value="{{ $empresa->telefono_2 }}">
						<small class="form-text text-muted">Escriba otro teléfono fijo (opcional).</small>
					</div>
			        <div class="form-group">
						<label for="">Email</label>
						<input type="text" class="form-control form-control-sm" name="email" 
									 aria-describedby="Titulo" value="{{ $empresa->email }}">
						<small class="form-text text-muted">Ej: organizacion@mail.com.</small>
					</div>
			        <div class="form-group">
						<label for="">Horario de atención</label>
						<textarea class="form-control" name="texto" id="" cols="30" rows="3">{{ $empresa->horario }}</textarea>
						<small id="" class="form-text text-muted">Ej: De Lunes a Viernes de 7:00 am a 7:00 pm jornada continua.</small>
					</div>        
				</div><!---------------------------- Fin columna derecha--------------------------->
			</div>
		</form>
	</div>
  </div>

@endsection
</section>
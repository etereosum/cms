
  
 
@include('librerias.libreria')
@include('librerias.libreria_mostrar_mas')
<div class="mostrar">
<p>Era una mañana fría de invierno cuando amanecí en ese pequeño cubículo de hospital, una cortina verde roída cubría la mitad de la vista que tenía del resto de la enorme habitación que compartía con otras cinco camas. Recordaba el día anterior claramente, me había internado por la tarde al romper fuente y esperé un rato antes de pasar al  quirofano para dar a luz a mi primer hijo.
	<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora, accusantium, alias, eius pariatur ipsa iure placeat minima vero quaerat accusamus sunt ipsam quasi quisquam voluptatibus odit in reiciendis suscipit nostrum?</span>
	<span>Nisi minima, reiciendis nemo commodi sapiente veniam veritatis qui ullam magnam vero. Dolor enim obcaecati eveniet minus in cumque eos reiciendis, ex impedit quidem, blanditiis corrupti saepe nobis voluptate iste!</span>
	<span>Vitae veritatis accusantium quam sed mollitia quod corrupti qui, est ipsa aperiam, quibusdam. Adipisci eius quo reiciendis soluta, modi. Asperiores ipsam nulla suscipit possimus modi recusandae odit maiores neque officia!</span>
</p>

<p>Cuando un gran amigo nos ofende, deberemos escribir en la arena donde el viento del olvido y el perdón se encargarán de borrarlo y apagarlo; por otro lado cuando nos pase algo grandioso, deberemos grabarlo en la piedra de la memoria del corazón donde viento ninguno en todo el mundo podrá borrarlo.</p>
</div>




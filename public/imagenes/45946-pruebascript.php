<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pruebaScript extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->database();
		$this->load->library('parser');
		$this->load->helper('url');
		
		$this->load->model('ModeloCRUD');
		$id=$this->session->userdata('idUsuario');
        if($id !=1){
          redirect('saludPublica/Alerta');

        }
	}
	
	public function index() {
		$data = array('titulo' => "Grupo de procesos",
				'elementosCrear' => $this->getControles(false),
				'elementosActualizar' => $this->getControles(true),
				//'tabla' => $this->getTablaRegistros(true),
				'scripts' => $this->getScripts(),
				'usuario' => $this->session->userdata('loginUsuario'),
				'navegador' => $this->session->userdata('navegador'),
				'onload' => 'onload="manejarSelect(false)"',
				'createAction' => 'pruebaScript/crear',
				'updateAction' => 'pruebaScript/actualizar',
				'deleteAction' => 'pruebaScript/borrar');
		
		echo $this->session->userdata('error');
		$this->session->set_userdata('error','');
		
		$this->parser->parse("templates/principalGenerico",$data);
		echo "<script>";
		echo "activarBoton()";
		echo "</script>";

	}
	
	
	private function getMenu() {
		return $this->session->userdata('menuNav');
	}
	
	public function getControles($actualizar) {

		$codigoHtml = "";
		
		if($actualizar) {
			$codigoHtml = "<div class=\"form-group\">
						       <label class=\"form-label\" for=\"idg\">C&oacute;digo grupo de procesos</label>
						       <div class=\"controls\">
						   	       <input class=\"form-control\" type=\"text\" name=\"idg\" id=\"idg\" style=\"width: 100%;\" autofocus readonly>
						       </div>";
		}
		else
			$codigoHtml = "";
			
			$codigoHtml .= "<div class=\"form-group\">
					       <label class=\"form-label\" for=\"".($actualizar ? "nomg" : "noma")."1\">Nombre grupo procesos</label>
					       <div class=\"controls\">
	    		               <input class=\"form-control\" type=\"text\" id=\"".($actualizar ? "nomg" : "noma")."\" name=\"".($actualizar ? "nomg" : "nombrec")."\" placeholder=\"Nombre grupo procesos\" maxlength=\"300\"
	    		               		  autofocus required/>
						   </div>
					   </div>
	    		       <div class=\"form-group\">
					       <label class=\"form-label\" for=\"".($actualizar ? "macro2" : "macro")."1\">Macroproceso</label>
					       <div class=\"controls\">
	    		               <select class=\"form-control\" id=\"".($actualizar ? "macro2" : "macro")."\" onclick=\"manejarSelect()\" name=\"".($actualizar ? "macroa" : "macroc")."\">";
			$id=$this->session->userdata('idUsuario');
			foreach($this->ModeloCRUD->getMacroProcesos() as $a) {
				$codigoHtml .= "<option value=\"".$a['mcp_id']."\">".$a['mcp_id']." - ".$a['mcp_nombre']."</option>";
			}
			
			$codigoHtml .= "</select>
				

			
						   	       <input class=\"form-control\" type=\"hidden\" name=\"idUser\" id=\"idUser\" style=\"width: 100%;\" value=\"$id\"y>
						     
		
						</div></div>";
			
			return $codigoHtml;
	}
	
	

	
	
	private function getScripts() {

		$script = "<script>
		function activarBoton(){
			var identificador = $(\"#idUser\").val();
			if (identificador !=2){
				$(\"#limpiar\").attr(\"disabled\", true);
				$(\"#crear\").attr(\"disabled\", true);
			}
			
		}
		window.onload=activarBoton	
				   </script>
				";
		
		return $script;
	}
	

                
	

}
<?php


//HOME
Route::get('/', 'HomeController@index')->name('home');

//PUBLIC

Route::group(['prefix' => 'public'],function(){

    //LOGIN
    Route::post('login','Auth\LoginController@login')->name('login'); 
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    //JUNTA
    Route::get('junta/public','JuntaController@junta_public')->name('junta.public.index');

    //HOME
    Route::get('home/noticia/show/{noticia_id}','HomeController@show')->name('home.noticia.show');
    Route::get('home/noticias/list','HomeController@list')->name('home.noticias.list');

    //SEARCH
    Route::get('search/get/{string?}','SearchController@get')->name('search.get');
    Route::get('search/show/{pos_id}','SearchController@show')->name('search.show');

    //QUIENES SOMOS (INDEX 2)
    Route::get('quienes-somos','QuienesSomosController@index_public')->name('quienes-somos.index2');

    //contactos
    Route::post('contactos/send_message','ContactoController@send_message')->name('contactos.send_message');
    Route::get('contactos','ContactoController@index')->name('contactos.index');

    //documents
    Route::get('noticias/show_document/{id}/{nombre}','NoticiaController@show_document')
        ->name('public.document-pos.show');

    //SGSST
    Route::get('sgsst/index','SgsstController@index')->name('sgsst.index');

    //PLAN DE ACCION
    Route::get('plan_accion/index','PlanAccionController@index')->name('plan_accion.index');
    //QUIENES SOMOS (INDEX 2)
    Route::get('post_email/{pos_id}','NoticiaController@post_email')->name('post_email.publicaciones');

    Route::get('normativa_public','NormativaController@index_public')->name('normativa_public.index');



});

//ADMIN

Route::group(['prefix' => 'admin'],function(){

    //CORREOS

    //Route::post('correos/upload','CorreoController@upload')->name('correos.upload');
    Route::get('correos/search/{elemento?}','CorreoController@search');
    Route::get('correos/list','CorreoController@list');
    Route::resource('correos','CorreoController');
    Route::get('correos/destroy/{correo_id}','CorreoController@destroy');
    
    //OTROS
    Route::get('otros/delete_doc/{obj_id}','OtroController@delete_doc')->name('otros.delete-pos');
    Route::get('otros/index','OtroController@index')->name('otros.index');
    Route::get('otros/edit_pos/{pos_id}','OtroController@editar_pos')->name('otros.edit_pos');
    Route::get('otros/edit_element/{element_id}','OtroController@editar_element')->name('otros.edit_element');
    Route::put('otros/update/{id}','OtroController@update')->name('otros.update');
    Route::get('otros/{object_id}/ver/{nombre}','OtroController@show_document')->name('document-pos.show');
 
    //NORMATIVA
    Route::get('normativa/delete_img/{normativa_id}','NormativaController@delete_image')->name('normativa.delete_img');
    Route::get('normativa/delete_doc/{obj_id}','NormativaController@delete_doc')->name('normativa.delete-pos');
    Route::resource('normativa','NormativaController');
    Route::get('normativa/{object_id}/ver/{nombre}','NormativaController@show_document')->name('document-normativa.show');
    Route::get('normativa/destroy/{normativa_id}','NormativaController@destroy')->name('normativa.destroy');
    Route::get('normativa/get_documento/{pos_id}','NormativaController@get_documento')->name('normativa.get_document');
    
    //ESTATUTOS
    Route::get('estatutos/delete_img','EstatutosController@delete_image')->name('estatutos.delete_img');
    Route::resource('estatutos','EstatutosController');

    //JUNTA
    Route::resource('junta','JuntaController');
    Route::get('junta/destroy/{integrante_id}','JuntaController@destroy')->name('junta.destroy');
    
    //EMPRESA
    Route::resource('empresa','EmpresaController');
    
    //HISTORIA
    Route::get('historia/delete_img','HistoriaController@delete_image')->name('historia.delete_img');
    Route::resource('historia','HistoriaController');
    
    //USER
    Route::get('users/edit_pass/{user_id}','UserController@edit_pass')->name('users.edit_pass');
    Route::post('users/update_pass','UserController@update_pass')->name('users.update_pass');
    Route::resource('users','UserController');
    Route::get('users/{user_id}/destroy','UserController@destroy')->name('users.destroy');
    
    //NOTICIAS
    Route::get('noticia/delete_doc/{obj_id}','NoticiaController@delete_doc')->name('noticia.delete-pos');
    Route::get('noticia/delete_img/{noticia_id}','NoticiaController@delete_image')->name('noticia.delete_img');
    Route::get('noticias/send/{noticia_id}','NoticiaController@send')->name('noticias.send');
    Route::resource('noticias','NoticiaController');
    Route::get('noticias/{object_id}/ver/{nombre}','NoticiaController@show_document')->name('document-noticia.show');
    Route::get('noticias/get_document/{pos_id}','NoticiaController@get_document')->name('noticias.get_document');
    Route::get('noticias/destroy/{noticia_id}','NoticiaController@destroy')->name('noticias.destroy');
   
    //DASHBOARD
    Route::get('dash','DashboardController@index')->name('dash.index');
    
    //BANNER
    Route::resource('banner','BannerController');
    Route::get('banner/destroy/{banner_id}','BannerController@destroy')->name('banner.destroy');
    Route::get('banner/get_banner/{banner_id}','BannerController@get_banner')->name('banner.get_banner');

    //LINKS
    Route::resource('links','LinkController');
    Route::post('links/actualizar','LinkController@actualizar')->name('links.actualizar');
    Route::get('links/destroy/{link_id}','LinkController@destroy')->name('link.destroy');
    
    //QUIENES SOMOS
    Route::resource('quienes_somos','QuienesSomosController');

    //HISTORIA
    Route::resource('historia','HistoriaController');

    //PERFIL
    Route::get('perfil','HomeController@get_perfil')->name('perfil');

    //INSTITUCION
    Route::get('institucion/create','InstitucionController@create')->name('institucion.create');
    Route::post('institucion','InstitucionController@store')->name('institucion.store');

    //HORIZONTE
    Route::put('horizonte/{horizonte}','HorizonteController@update')->name('horizonte.update');

    //ESTATUTOS
    Route::get('estatutos/delete_doc/{obj_id}','EstatutosController@delete_doc')->name('estatutos.delete-pos');
    Route::get('estatutos/get_document/{pos_id}','EstatutosController@get_document')->name('estatutos.get_document');
    Route::get('estatutos/{object_id}/ver/{nombre}','EstatutosController@show_document')->name('document-estatutos.show');

});

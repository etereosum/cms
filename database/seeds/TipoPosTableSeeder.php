<?php

use Illuminate\Database\Seeder;
use App\TipoPos;

class TipoPosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoPos::create(['nombre' => 'Noticia']); //1
        TipoPos::create(['nombre' => 'Historia']); //2
        TipoPos::create(['nombre' => 'Horizonte institucional']); //3
        TipoPos::create(['nombre' => 'Estatutos']); //4
        TipoPos::create(['nombre' => 'Normativa']); //5
        TipoPos::create(['nombre' => 'Plan de accion']); //6
        TipoPos::create(['nombre' => 'SGSST']); //7
    }
}

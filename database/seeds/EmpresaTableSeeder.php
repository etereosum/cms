<?php

use Illuminate\Database\Seeder;
use App\Empresa;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresa::create([
            'id'         => 1,
            'nombre'     => 'Asociación de docentes directivos de Risaralda "ASODIR"',
            'nit'        => '800188993-8',
            'telefono_1' => '3350544',
            'telefono_2' => '000000000',
            'celular_1'  => '3105469987',
            'celular_2'  => '000000000',
            'direccion'  => 'Carrera 5 No. 22-20 Oficina 408',
            'email'      => 'asodirrisaralda@gmail.com',
            'horario'    => 'Luneas a vienes de 8 a.m a 12 m y 2 p.m a 6 p.m'
        ]);
    }
}

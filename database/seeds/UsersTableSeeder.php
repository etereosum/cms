<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombres' => 'Pablo Adrian',
                'apellidos' => 'Gonzalez Salazar',
                'institucion' => 'Universidad tecnológica de pereira',
                'rol' => 'Administrador',
                'estado' => 'Activo',
                'email' => 'etereosum@gmail.com',
                'telefono' => '3334565464',
                'posicion' => NULL,
                'cargo_id' => NULL,
                'user_create_id' => NULL,
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:09:32',
                'updated_at' => '2020-10-25 11:09:32',
                'email_verified_at' => '2020-10-25 11:09:32',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => 'u1xwFOojsz',
            ),
            1 => 
            array (
                'id' => 2,
                'nombres' => 'Lesny',
                'apellidos' => 'Mosquera Renteria',
                'institucion' => 'Universidad tecnológica de pereira',
                'rol' => 'Administrador',
                'estado' => 'Activo',
                'email' => 'mosqueralesny@hotmail.com',
                'telefono' => '3117761536',
                'posicion' => NULL,
                'cargo_id' => NULL,
                'user_create_id' => NULL,
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:09:32',
                'updated_at' => '2020-10-25 11:09:32',
                'email_verified_at' => '2020-10-25 11:09:32',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => '53Kuuy4PbH',
            ),
            2 => 
            array (
                'id' => 3,
                'nombres' => 'Pablo',
                'apellidos' => 'Gonzalez',
                'institucion' => 'Inem Felipe Perez',
                'rol' => 'Junta directiva',
                'estado' => 'Activo',
                'email' => 'mail@gmail.com',
                'telefono' => '333333333',
                'posicion' => 1,
                'cargo_id' => 1,
                'user_create_id' => NULL,
                'user_update_id' => 1,
                'created_at' => '2020-10-25 11:09:32',
                'updated_at' => '2020-10-25 11:24:31',
                'email_verified_at' => '2020-10-25 11:09:32',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => 'MSfaDe2L3n',
            ),
            3 => 
            array (
                'id' => 4,
                'nombres' => 'Martha Lucia',
                'apellidos' => 'Salazar Rojas',
                'institucion' => 'Diocesano',
                'rol' => 'Junta directiva',
                'estado' => 'Activo',
                'email' => 'maierl@gmail.com',
                'telefono' => '333333333',
                'posicion' => 2,
                'cargo_id' => 2,
                'user_create_id' => NULL,
                'user_update_id' => 1,
                'created_at' => '2020-10-25 11:09:32',
                'updated_at' => '2020-10-25 11:24:51',
                'email_verified_at' => '2020-10-25 11:09:32',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => 'Cb7Y6LhHKW',
            ),
        ));
        
        
    }
}
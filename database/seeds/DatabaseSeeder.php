<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DeptoTableSeeder::class);
        $this->call(MunicipioTableSeeder::class);
        $this->call(CargoTableSeeder::class);
        $this->call(InstitucionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TipoPosTableSeeder::class);
        $this->call(PosTableSeeder::class);
        $this->call(EmpresaTableSeeder::class);
        $this->call(ElementosTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Cargo;

class CargoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cargo::create([
            'id'        => 1,
            'nombre'    => 'Secretario'
        ]);

        Cargo::create([
            'id'        => 2,
            'nombre'    => 'Presidente'
        ]);

        Cargo::create([
            'id'        => 3,
            'nombre'    => 'Vocal'
        ]);
    }
}

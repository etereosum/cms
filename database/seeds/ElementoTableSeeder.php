<?php


use Illuminate\Database\Seeder;

use App\Elemento;

class ElementoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Elemento::create([
            'id'        => 1,
            'nombre'    => 'Facebook',
            'tipo'      => 'Facebook',
            'posicion'  => 1,
            'ruta'      => 'img/redes/facebook.png',
            'link'      => 'https://www.facebook.com/VivaAirCo'
        ]);
    }
}

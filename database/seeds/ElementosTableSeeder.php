<?php

use Illuminate\Database\Seeder;

class ElementosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('elementos')->delete();
        
        \DB::table('elementos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'img5.jpg',
                'estado' => 'Activo',
                'posicion' => 4,
                'link' => NULL,
                'tipo' => 'Banner',
                'ruta' => 'img/banner/p0Fe7AkyNdkkZT3fwiXyUpS2jOpYpkUjDiAZENVU.jpeg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:16:45',
                'updated_at' => '2020-10-25 11:16:45',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'img6.jpg',
                'estado' => 'Activo',
                'posicion' => 3,
                'link' => NULL,
                'tipo' => 'Banner',
                'ruta' => 'img/banner/FWuBgdjEepBHzPkf8iiu87a60NW9jkxoMZhVBQnV.jpeg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:16:53',
                'updated_at' => '2020-10-25 11:16:53',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'img7.jpg',
                'estado' => 'Activo',
                'posicion' => 2,
                'link' => NULL,
                'tipo' => 'Banner',
                'ruta' => 'img/banner/3PfHU64I1YVoYg7Ns34KmUNvl025G78PlS073xa9.jpeg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:17:00',
                'updated_at' => '2020-10-25 11:17:00',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'img9.jpg',
                'estado' => 'Activo',
                'posicion' => 1,
                'link' => NULL,
                'tipo' => 'Banner',
                'ruta' => 'img/banner/fuxU9De1MnTfxSwzlMPTIoLdK06pyaSNLxwnphwe.jpeg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:17:08',
                'updated_at' => '2020-10-25 11:17:08',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'img10.jpg',
                'estado' => 'Activo',
                'posicion' => 5,
                'link' => NULL,
                'tipo' => 'Banner',
                'ruta' => 'img/banner/JoYLrCT4lIlHzHe37FCA6Dx0HrZzRlDjVjqwLDn6.jpeg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:17:20',
                'updated_at' => '2020-10-25 11:17:20',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'cnsc.svg',
                'estado' => 'Activo',
                'posicion' => 7,
                'link' => NULL,
                'tipo' => 'Link',
                'ruta' => 'img/links/Mq6H7mIvvYdRyJ0pDtOPbroksEy5MtXdovojmw6M.svg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:18:30',
                'updated_at' => '2020-10-25 11:18:30',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'icfes.svg',
                'estado' => 'Activo',
                'posicion' => 6,
                'link' => NULL,
                'tipo' => 'Link',
                'ruta' => 'img/links/pMzTxMs3BLF2Hu1wwhjQQ2f1mzJqADQUZqVnfbjE.svg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:18:37',
                'updated_at' => '2020-10-25 11:18:37',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'MEN.svg',
                'estado' => 'Activo',
                'posicion' => 5,
                'link' => NULL,
                'tipo' => 'Link',
                'ruta' => 'img/links/5SOPYtKlZAlKCvcfAbdbaMQB8Hc42USgmWm9XBNP.svg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:18:42',
                'updated_at' => '2020-10-25 11:18:42',
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'OSrxsoDQNoBeFoWkXZwTJ9VSIdnFQuBlIBgq8VAd.svg',
                'estado' => 'Activo',
                'posicion' => 4,
                'link' => NULL,
                'tipo' => 'Link',
                'ruta' => 'img/links/jTOgmwBbSpplXwkAFYkj16IyTV0k0UYsPOi7dpdb.svg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:18:49',
                'updated_at' => '2020-10-25 11:18:49',
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'sem_dosquebradas.svg',
                'estado' => 'Activo',
                'posicion' => 3,
                'link' => NULL,
                'tipo' => 'Link',
                'ruta' => 'img/links/1a94kGUFnxYtNEnX9H0f4pNnZa6E4tTbzJed0knX.svg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:18:56',
                'updated_at' => '2020-10-25 11:18:56',
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'sena.svg',
                'estado' => 'Activo',
                'posicion' => 2,
                'link' => NULL,
                'tipo' => 'Link',
                'ruta' => 'img/links/cRckxBcJwLTM2LT6SyzEoeicD2JesDr4xeUoXLk0.svg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:19:04',
                'updated_at' => '2020-10-25 11:19:04',
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'TbaI4uL60Jb7LYTjhUVaVlO0fPVT5SxT2BGexDju.svg',
                'estado' => 'Activo',
                'posicion' => 1,
                'link' => NULL,
                'tipo' => 'Link',
                'ruta' => 'img/links/sh03vU25xs6AlQJNTyEnUAk6iHGzrRtINt8SEJSZ.svg',
                'user_update_id' => NULL,
                'created_at' => '2020-10-25 11:19:14',
                'updated_at' => '2020-10-25 11:19:14',
            ),
        ));
        
        
    }
}
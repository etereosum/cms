<?php

use Faker\Generator as Faker;
use App\Pos;

$factory->define(Pos::class, function (Faker $faker) {
    return [
        'img_name'      => 'img123.jpeg',
        'img_ruta'      => 'img/img123.jpeg',
        'texto'         => $faker->sentence($nbWords = 300, $variableNbWords = true),
        'titulo'        => $faker->sentence($nbWords = 5, $variableNbWords = true),
        'doc_name'      => 'adjunto.pdf',
        'doc_ruta'      => 'adjuntos/fbwWqvXfnkxEPAlaOEmO4eP94M9xWMxaNqSdjcCw.pdf',
        'tipo_pos_id'   => $faker->numberBetween($min=1,$max=5),
        'user_create_id'=> 1
    ];
});

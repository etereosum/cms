<?php

use Faker\Generator as Faker;
use App\Depto;

$factory->define(Depto::class, function (Faker $faker) {
    return [
        'nombre' => $faker->state,
    ];
});

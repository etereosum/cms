<?php

use Faker\Generator as Faker;
use App\Institucion;

$factory->define(Institucion::class, function (Faker $faker) {
    return [
        'nombre'    => $faker->company,
        'direccion' => $faker->streetAddress,
        'municipio_id'  => $faker->numberBetween($min=1, $max=200),
        'telefono'  => $faker->phoneNumber,
        'email'     => $faker->unique()->email
    ];
});

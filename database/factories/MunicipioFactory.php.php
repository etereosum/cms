<?php

use Faker\Generator as Faker;
use App\Municipio;

$factory->define(Municipio::class, function (Faker $faker) {
    return [
        'nombre' => $faker->city,
        'depto_id' => $faker->numberBetween($min = 1, $max = 20)
    ];
});

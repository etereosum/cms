<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('estado',['Activo', 'Inactivo'])->default('Activo');
            $table->string('img_name')->nullable();
            $table->string('img_ruta')->nullable();
            $table->longText('texto')->nullable();
            $table->string('titulo')->nullable();
            $table->string('doc_name')->nullable();
            $table->string('doc_ruta')->nullable(); 
            $table->unsignedBigInteger('tipo_pos_id'); 
            $table->unsignedBigInteger('user_create_id');
            $table->unsignedBigInteger('user_update_id')->nullable();
            $table->timestamps();

            $table->foreign('user_create_id')->references('id')->on('users');
            $table->foreign('user_update_id')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos');
    }
}

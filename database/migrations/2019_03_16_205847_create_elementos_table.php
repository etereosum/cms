<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elementos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->enum('estado',['Activo','Inactivo'])->default('Activo');
            $table->integer('posicion')->nullable();
            $table->string('link')->nullable();
            $table->enum('tipo',[   
                                    'Banner',
                                    'Facebook',
                                    'Link',
                                    'Slider',
                                    'Twitter',
                                    'Youtube' 
                                ]);
            $table->string('ruta')->nullable()->unique();
            $table->unsignedBigInteger('user_update_id')->nullable();
            $table->timestamps();

            $table->foreign('user_update_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elementos');
    }
}
